<?php
    require_once __DIR__."/../../service/ContactService.class.php";
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../service/PictureService.class.php";
    require_once __DIR__."/../../utility/Utility.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";

    try {

	    
	    require "component/userAuth.php";
	    require "component/userProfilePic.php";
		
        $pid = isset($_GET["id"])? $_GET["id"]: $mid;
        $user = $accountService->load($pid);
	    
        $contact = $contactService->loadByUserId($user->id);
				
        $pictures = $pictureService->getUserPhotos($contact->id);
		
		//remove all the private pictures if they don't belong to the current user
		if($me->id != $user->id) {
			foreach($pictures as $key => $p) {
				if($p->visibility==1) {
					unset($pictures[$key]);
				}
				
				$pictures = array_values($pictures);
			}
			
		}
	}
	catch(Exception $e) {
        Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$e->getMessage(),false);
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Velidate</title>
        <link href="../libs/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/main.css" rel="stylesheet">
        <link href="../css/components.css" rel="stylesheet">
        <link rel="stylesheet" href="../libs/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="../libs/owl-carousel/owl.theme.css">
    </head>

     <body>
        <?php require "component/analyticstracking.php"?>
        <div id="message-box" class="alert alert-info"></div>
        <div id="error-box" class="alert alert-danger"></div>
        <div class="site-wrapper for-footer-header">
            <div class="site-wrapper-inner">
                <?php require "component/navigation.php"; ?>
                <div class="main-content">
                    <div class="content-container">
                    	<?php 
                    		if($me->id == $user->id) {
                    	?>
                    		<h2 class="picture-title">Upload Your Best Photos!</h2>
                    		<div class="picture-subtitle">These are your photos. Upload more and set the best one as the main photo.</div>
                    		<div class="picture-profile-link"><a href="<?php echo Properties::$PROFILE_PAGE ?>">Back to My Profile</a></div>
                    	<?php
                    		}
                    	?>
                    	
                        <?php if(count($pictures)==0) { ?>
                        <div  style="text-align:center;"class="message-container need-footer need-header">
                            <div><img src="../imgs/heart_img.png" /></div>
                            <?php if($me->id != $user->id) { ?>
                            <p class="msg" style="text-align:center; margin-top:20px;">This user doesn't have any pictures uploaded. 
                            </p>
                            <?php } else { ?>
                            <p class="msg" style="text-align:center; margin-top:20px;">You don't have any pictures uploaded yet. </p>
                            <p class="msg" style="text-align:center; margin-top:40px;"><a class="button_s blue" href="<?php echo Properties::$UPLOAD_PHOTO_PAGE ?>">Upload Your First Photo Now</a></p>
                            <?php } ?>
                        </div>
                        <?php }else{ ?>
                            <?php if($me->id == $user->id) { ?>
	                        
                            <div class="photo-manage-group">
	                            <div id="upload-button" class="photo-button"><a class="button_sss blue" href="<?php echo Properties::$UPLOAD_PHOTO_PAGE ?>">Upload New Photo</a></div>
	                            <div id="set-main-button" class="photo-button"><a class="button_sss blue" href="">Set as Main Photo</a></div>
	                            <div style="display:none" id="set-private-button" class="photo-button"><a class="button_sss blue" href="">Set as Private</a></div>
	                            <div style="display:none" id="set-public-button" class="photo-button"><a class="button_sss blue" href="">Set as Public</a></div>
	                            <div id="delete-button" class="photo-button"><a class="button_sss blue" href="">Delete</a></div>
	                        </div> 
                        	<?php } ?>
                        <div id="owl-demo" class="owl-carousel">
                            <?php for ($i=0; $i<count($pictures); $i++) { 
                                ?>  <div id="pic-<?php echo $pictures[$i]->id ?>" class="pic-item <?php if($pictures[$i]->isMainPhoto==1) echo "main-photo"?>">
                                		<img src='<?php echo $pictures[$i]->linkMedium ?>'>
                                		
                            			<?php if($me->id == $user->id) { ?>
                                			<div class="pic-status"><div class="inline-pic-status" data-id="<?php echo $pictures[$i]->id ?>"><span class="is-main" >Main |</span> <span class="visibility">Public</span></div></div>
                                		<?php } ?>
                                	</div> 
                                <?php
                             } 
                             ?>
                        </div>
                        <?php } ?>
                        
                    </div>
                </div>
            </div>
        </div>
        <?php require "component/footer.php"?>
    </body>
    <script src="../libs/jquery-1.11.1.min.js"></script>
    <script src="../libs/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    <script src="../libs/owl-carousel/owl.carousel.js"></script>
    <script src="../libs/bootbox.min.js"></script>
    
    <?php if($me->id != $user->id) { ?> 
    <script>
    	$(document).ready(function() {
 
          $("#owl-demo").owlCarousel({
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
    		autoHeight : true,
            lazyLoad : true,
            singleItem:true
          });
          
        });
    </script>
    <?php }?>
    <?php if($me->id == $user->id) { ?>
    <script>
    	var pics = JSON.parse('<?php echo json_encode($pictures)?>');
    	var photoLock = true;
    	function deletePicture(pid) {
    		if(photoLock)
    			photoLock=false;
    		else
    			return;
    			
    		data = {pid:pid}
    		$.ajax({
                	type: "GET",
                	url: "<?php echo Properties::$PHOTO_DELETE_ACTION ?>",
                	dataType: "text",
                	data: data,
                	success: function(data) {
            			$("#message-box").show();
            			$("#message-box").text("Update completed.");
            			$("#message-box").delay(2000).fadeOut(1000);
            			
            			var pic = null;
            			var mainIndex = -1;
            			for(var i=0; i<pics.length; i++) {
            				if(pics[i].id == pid) {
            					mainIndex = i;
            				}
            			}
            			
            			if(mainIndex == -1)
            				return;
            			
    					pics.splice(mainIndex,1);
    					$("#owl-demo").data('owlCarousel').removeItem(mainIndex);
            			
    					photoLock = true;
            			
                	},
                	error: function(xhr, status, error) {
            			$("#error-box").show();
            			$("#error-box").text(xhr.responseText);
            			$("#error-box").delay(2000).fadeOut(1000);
            			
            			
    					photoLock = true;
					}
            	});
    	}
    	
    	function setVisibility(pid,visibility) {
    		if(photoLock)
    			photoLock=false;
    		else
    			return;
    		data = {pid:pid, visibility: visibility}
    		$.ajax({
                	type: "GET",
                	url: "<?php echo Properties::$PHOTO_SET_VISIBILITY_ACTION ?>",
                	dataType: "text",
                	data: data,
                	success: function(data) {
            			$("#message-box").show();
            			$("#message-box").text("Update completed.");
            			$("#message-box").delay(2000).fadeOut(1000);
            			
            			var pic = null;
            			for(var i=0; i<pics.length; i++) {
            				if(pics[i].id == pid) {
            					pics[i].visibility = visibility;
            					updateUI(i);
            				}
            			}
            			
    					photoLock=true;
                	},
                	error: function(xhr, status, error) {
            			$("#error-box").show();
            			$("#error-box").text(xhr.responseText);
            			$("#error-box").delay(2000).fadeOut(1000);

    					photoLock=true;					
    				}
            	});
    	}
    	
    	function setMainPhoto(pid) {
    		if(photoLock)
    			photoLock=false;
    		else
    			return;
    		
    		data = {pid:pid}
    		$.ajax({
                	type: "GET",
                	url: "<?php echo Properties::$PHOTO_SET_MAIN_PHOTO_ACTION ?>",
                	dataType: "text",
                	data: data,
                	success: function(data) {
            			$("#message-box").show();
            			$("#message-box").text("Update completed.");
            			$("#message-box").delay(2000).fadeOut(1000);
            			
            			var pic = null;
            			var mainIndex = 0;
            			for(var i=0; i<pics.length; i++) {
            				if(pics[i].id == pid) {
            					pics[i].isMainPhoto = 1;
            					mainIndex = i;
            					$(".profile-icon img").attr("src",pics[i].linkSmall);
            				}
            				else {
            					pics[i].isMainPhoto = 0;
            				}
            			}
            			
            			updateUI(mainIndex);
            			
    					photoLock=true;
            			
                	},
                	error: function(xhr, status, error) {
            			$("#error-box").show();
            			$("#error-box").text(xhr.responseText);
            			$("#error-box").delay(2000).fadeOut(1000);
            			
            			
    					photoLock=true;
					}
            	});
    	}
    	
    	function updateUI(i) {
    		var pic = pics[i];
    		if (pic.isMainPhoto == 0) {
    			$(".inline-pic-status[data-id='"+pic.id+"'] .is-main").hide();
    			$("#set-main-button").show();
    			
    			$('#set-main-button').unbind('click');
    			$("#set-main-button").click(function() {
    				warning = "<h3>Warning!</h3><p>You will lose your verification of picture if you change your main photo. Are you sure to proceed?</p>"
    				bootbox.confirm(warning, function(result) {
	            		if(result) {
    						setMainPhoto(pic.id);
	            		}
					});
    				
    				return false;
    			});
    			
    			$("#delete-button").show();
    			$("#delete-button").unbind('click');
    			$("#delete-button").click(function() {
    				warning = "<h3>Warning!</h3><p>Are you sure that you want to delete the picture?</p>"
    				bootbox.confirm(warning, function(result) {
	            		if(result) {
    						deletePicture(pic.id);
	            		}
					});
    				
    				return false;
    			});
    		}
    		else {
    			$(".inline-pic-status[data-id='"+pic.id+"'] .is-main").show();
    			$('#set-main-button').unbind('click');
    			$("#set-main-button").click(function() {
    				warning = "<h3>Tips</h3><p>This picture is already your main picture.</p>"
    				bootbox.alert(warning);
    				
    				return false;
    			});
    			
    			$("#delete-button").show();
    			$("#delete-button").unbind('click');
    			$("#delete-button").click(function() {
    				warning = "<h3>Tips</h3><p>A main picture could not be deleted. If you really want to delete the picture, please set another picture as your main picture, then delete this one.</p>"
    				bootbox.alert(warning);
    				
    				return false;
    			});
    		}
    		
    		if (pic.visibility == 1) {
    			$(".inline-pic-status[data-id='"+pic.id+"'] .visibility").text("Private");
    			$("#set-public-button").show();
    			$("#set-private-button").hide();
    			
    			$('#set-public-button').unbind('click');
    			$("#set-public-button").click(function() {
    				setVisibility(pic.id,0);
    				return false;
    			});
    		}
    		else {
    			$(".inline-pic-status[data-id='"+pic.id+"'] .visibility").text("Public");
    			$("#set-public-button").hide();
    			$("#set-private-button").show();
    			
    			$('#set-private-button').unbind('click');
    			$("#set-private-button").click(function() {
    				setVisibility(pic.id,1);
    				return false;
    			});
    		}
    		
    		if (pic.isMainPhoto == 1) {
    			$('#set-private-button').unbind('click');
    			$("#set-private-button").click(function() {
    				warning = "<h3>Tips</h3><p>This picture is your main picure. A private picture can not be a main picture. If you really want to change it to private, please use another picture as your main picture first.</p>"
    				bootbox.alert(warning);
    				
    				return false;
    			});
    		}
    		if (pic.visibility == 1) {
    			$('#set-main-button').unbind('click');
    			$("#set-main-button").click(function() {
    				warning = "<h3>Tips</h3><p>A private picture can not be a main picture. Please change it to be public first.</p>"
    				bootbox.alert(warning);
    				
    				return false;
    			});
    		}
    	}
    	
        $(document).ready(function() {
 
          $("#owl-demo").owlCarousel({
    		afterAction : function() {
    			updateUI(this.owl.visibleItems);
    		},
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            lazyLoad : true,
    		autoHeight : true,
            singleItem:true
          });
          
          if(pics.length == 0) {
          	
          		$('#upload-button').hide();
    			$('#set-public-button').hide();
    			$('#set-private-button').hide();
    			$('#set-main-button').hide();
    			$("#delete-button").hide();
          } else {
          	if(window.location.hash == "#last") {
          		$("#owl-demo").trigger('owl.jumpTo',pics.length-1)
          	}
          }
          
          
        });
    </script>
    <?php }?>
	<?php require "component/messageTab.php" ?>
	<script>
	</script>
</html>