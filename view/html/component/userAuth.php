<?php
session_start();
$mid = $_SESSION['user_id'];
if ($mid == null) {
	Utility::redirect(Properties::$MESSAGE_PAGE . "?msg=Please login first", false);
}

$accountService = new AccountService();
$me = $accountService -> load($mid);

if ($me -> accountStatus != User::CONFIRMED) {
	$msg = "Hi, " . $me -> userName . ". Your account is not activiated yet. We have sent a confirmation email to you." . " Please click the link inside to activate your account." . "<br>No email recieved? click <a href='" . Properties::$RESEND_CONFIRMATION_ACTION . "?id=" . $mid . "'>here</a> to recieve the email again.";

	Utility::redirect(Properties::$MESSAGE_PAGE . "?msg=" . $msg, false);
}
?>