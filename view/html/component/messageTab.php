<script src="//js.pusher.com/2.2/pusher.min.js"></script>
<script>
	function setMessageTab(totalMessages) {
		$(".totalUnread").remove();
		if (totalMessages != 0) {
			$("#message-tab").append('<div class="totalUnread">' + totalMessages + '</div>')
		}
	}

	var myId =  <?php echo $mid ?>;
	var pusher = new Pusher('<?php echo Properties::$PUSHER_KEY ?>',{ authEndpoint: "<?php echo Properties::$PUSHER_AUTH_ACTION ?>
	" });
	var msgChannel = pusher.subscribe("private-msg-"+myId);

	var totalMessages = 0;
	$.ajax({
	url: "<?php echo Properties::$MESSAGE_GET_ALL_UNREAD_MESSAGE_COUNT_ACTION ?>",
	success: function(data) {
	totalMessages = data;
	setMessageTab(totalMessages);
	}
	});

	msgChannel.bind("new-message", function(data) {
	totalMessages++;
	setMessageTab(totalMessages);
	});
</script>