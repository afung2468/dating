<div class="header">
	<div class="header_container">
		<a class="logo" href="<?php echo Properties::$HOME_PAGE ?>"><img src="../imgs/logo_black.png" /></a>
		<div class="navi-group">
			<ul class="navi-tabs">
				<li>
					<a class="profile-icon" href="<?php  echo Properties::$PROFILE_PAGE?>"><img src="<?php echo $mMainPicLink ?>"></a>
				</li>
				<li class="nav-b" id="coins-tab">
					<a href="<?php  echo Properties::$LOGOUT_ACTION?>">Logout</a>
				</li>
				<li class="nav-b" id="edit-tab">
					<a href="<?php  echo Properties::$EDIT_PROFILE_PAGE?>" >Setting</a>
				</li>
				<li class="nav-b" id="message-tab">
					<a href="<?php  echo Properties::$CONVERSATION_PAGE?>">Message</a>
				</li>
				<li class="nav-b" id="search-tab">
					<a href="<?php  echo Properties::$SEARCH_PAGE?>" >Search</a>
				</li>
				<li class="nav-b" id="rate-tab">
					<a href="<?php  echo Properties::$RATING_PAGE?>#0" >Rate</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<script>
	function selectTab(id){
		$(id+" a").attr("id","selected");
	}
</script>