<?php
require_once __DIR__ . "/../../utility/Properties.class.php";
require_once __DIR__ . "/../../utility/Utility.class.php";
session_start();
$mid = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
if ($mid != null) {
	Utility::redirect(Properties::$PROFILE_PAGE);
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Velidate</title>
		<!-- Bootstrap core CSS -->
		<link href="../libs/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/home.css" rel="stylesheet">
		<link href="../css/components.css" rel="stylesheet">
		<link href="../css/font-awesome.css" rel="stylesheet">
		<link href="../css/bootstrap-social.css" rel="stylesheet">
	</head>

	<body>
        <?php require "component/analyticstracking.php"?>
		<div id="fb-root"></div>
		<script>
			( function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id))
						return;
					js = d.createElement(s);
					js.id = id;
					js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=143547199131394&version=v2.0";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
		</script>
		<div id="introduction" class="site-wrapper" style="position:relative">
			<div id="topper" style="position:absolute; top:0px; width:100%">
				<div style="margin-left:auto;margin-right:auto;width:1000px">
					<div style="float:right; margin-right:0px; padding:10px 20px; font-size: 14px; color:#fff; font-weight: bold; background-color: #336666;
					border-radius: 0px 0px 5px 5px;
					-webkit-border-radius: 0px 0px 5px 5px;
					-moz-border-radius: 0px 0px 5px 5px;
					-ms-border-radius: 0px 0px 5px 5px;
					-o-border-radius: 0px 0px 0px 5px">
						Exclusive to students in Ontario only
					</div>

					<div style="float:left; margin-left:100px; margin-top:8px">
						<div class="twitter-like" style="display:inline">
							<a href="/view/imgs/Funny-love-with-cat-dog.jpg"><img style="display:inline-block; vertical-align:top; margin-top:-6px; width:28px; height: 28px" src="../imgs/dog-icon.png"></a>
							<div class="fb-like" style="vertical-align:top;zoom:1;*display:inline" data-href="https://www.facebook.com/velidate" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
							<a class="twitter-follow-button" data-show-count="false" data-show-screen-name="false" href="https://twitter.com/velidatedotcom">Follow @velidatedotcom</a>
							<script>
								! function(d, s, id) {
									var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
									if (!d.getElementById(id)) {
										js = d.createElement(s);
										js.id = id;
										js.src = p + '://platform.twitter.com/widgets.js';
										fjs.parentNode.insertBefore(js, fjs);
									}
								}(document, 'script', 'twitter-wjs');
							</script>
						</div>
					</div>
				</div>
			</div>

			<div class="site-wrapper-inner">
				<div class="cover-container">
					<div class="logo fadeIn" style="opacity:0.1">
						<a href="<?php echo Properties::$HOME_PAGE ?>"><img src="../imgs/logo.png" /></a>
					</div>
					<div class="intro_text fadeIn" style="opacity:0.1">
						<h2>Meet Verified Students and Alumni</h2>
						<p>
							Velidate is a great place for chatting, making friends, and dating.
							<br>
							It’s 100% free.
						</p>
						<p>
							<a id="go-to-learn-more" class="button button_s white" href="#video">Learn More</a>
						</p>
					</div>
					<div class="intro_image lateFadeIn" style="opacity:0.1">
						<img src="../imgs/intro_image.png" />
					</div>
					<div class="intro_buttons marginIn" style="opacity:0.1;marginTop, 150px">
						<a class="button button_m white" href="<?php echo Properties::$SIGNUP_PAGE ?>">Signup</a>
						<a class="button button_m white" href="<?php echo Properties::$GET_FACEBOOK_USER_ACTION ?>">Login</a>
					</div>
				</div>
			</div>
		</div>
		<div id="video" class="site-wrapper">
			<div class="site-wrapper-inner">
				<div class="cover-container">
					<div class="intro_text fadeIn">
						<h2>Learn More</h2>
						<!--<p>Velidate is the unique online dating platform which verifies users profile to <br> help meet you with real singles around you</p>-->
					</div>
					<div class="marginIn">
						<a id="show-video-btn" href="#" class="button video_button white fadeIn"><img src="../imgs/video_button.png"></img></a>
					</div>
				</div>
			</div>
		</div>
		<div id="intro_rate" class="site-wrapper">
			<div class="site-wrapper-inner">
				<div class="cover-container">
					<div class="intro_text fadeIn">
						<h2>Have you been rated? Or want to rate someone anonymously?</h2>
						<p>
							Check out what all the hype is about!
						</p>
					</div>
					<div class="rating_image lateFadeIn">
						<img src="../imgs/rating_image.png" />
					</div>
					<div class="intro_buttons marginIn">
						<a class="button button_m blue" href="<?php echo Properties::$SIGNUP_PAGE ?>">Signup</a>
						<a class="button button_m blue" href="<?php echo Properties::$GET_FACEBOOK_USER_ACTION ?>">Login</a>
					</div>
				</div>
			</div>
		</div>
		<div id="intro_verification" class="site-wrapper">
			<div class="site-wrapper-inner">
				<div class="cover-container ">
					<div class="intro_text lateFadeIn">
						<h2>Velidate is a safe way to meet people</h2>
						<p>
							Velidate is the only dating site that filters out fake accounts from the real ones. This insures that you meet real singles around you.
						</p>
					</div>
					<div id="verification_img" class="fadeIn">
						<img src="../imgs/verification_image.png" />
					</div>
					<div class="intro_buttons lateFadeIn">
						<a class="button button_m white" href="<?php echo Properties::$SIGNUP_PAGE ?>">Signup</a>
						<a class="button button_m white" href="<?php echo Properties::$GET_FACEBOOK_USER_ACTION ?>">Login</a>
					</div>
				</div>
			</div>
		</div>
		<div id="get_start" class="site-wrapper">
			<div id="last_section" class="site-wrapper-inner">
				<div id="last_main" class="cover-container">
					<div class="intro_text">
						<h2>Find your someone special</h2>
					</div>
					<div class="intro_buttons">
						<a class="button button_m blue" href="#">Get Started</a>
					</div>
				</div>
				<div class="footer">
					<div class="footer_container">
						<!--
						<span class="footer-links">
						<a href="#">Help/FAQ</a>
						<a href="#">Success Stories</a>
						<a href="#">Press</a>
						</span>
						-->
						<span class="reserved-right">Copyright © 2014 vaidate. All rights reserved</span>
                        <!--
						<span class="reserved-right"><a href="//www.iubenda.com/privacy-policy/666629" class="iubenda-white iubenda-embed" title="Privacy Policy">Privacy Policy</a>
							<script type="text/javascript">
								(function(w, d) {
									var loader = function() {
										var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0];
										s.src = "//cdn.iubenda.com/iubenda.js";
										tag.parentNode.insertBefore(s, tag);
									};
									if (w.addEventListener) {
										w.addEventListener("load", loader, false);
									} else if (w.attachEvent) {
										w.attachEvent("onload", loader);
									} else {
										w.onload = loader;
									}
								})(window, document);
							</script> </span>
                            -->
                            <span class="reserved-right"><a href="#" onclick="window.open('http://velidate.com/privacy.html',
'mywindow','menubar=0,resizable=1,scrollbars=1,width=860,height=650')" title="Privacy Policy">Privacy Policy</a></span>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModelLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
						</button>
						<h4 class="modal-title" id="loginModelLabel">Welcome Back</h4>
					</div>
					<div class="modal-body">
						<div >
							<form action="login.php" class="login-form" role="form">
								<div class="by-facebook">
									<button onclick="window.location.href='<?php echo Properties::$GET_FACEBOOK_USER_ACTION ?>'" type="button" class="btn btn-block btn-social btn-facebook form-component">
										<i class="fa fa fa-facebook"></i> Login via Facebook
									</button>
								</div>
								<div>
									<hr>
								</div>

								<div id="email-group" class="form-group">
									<input name="email" type="email" class="form-control" id="email" placeholder="Email">
								</div>
								<div id="password-group" class="form-group">
									<input name="password" type="password" class="form-control" id="password" placeholder="Password">
								</div>
								<div class="error-group">
									<strong></strong>
								</div>
								<div id="login-submit-container" class="form-group">
									<a id="login-submit" class="button button_s blue">Login</a>
									<a id="login-cancel" data-dismiss="modal"  class="button button_s blue">Cancel</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a id="prev-page" class="navi-button"><img src="../imgs/previous-page.png"></a>
		<a id="next-page" class="navi-button"><img src="../imgs/next-page.png"></a>

		<script src="../libs/jquery-1.11.1.min.js"></script>
		<script src="../libs/placeholder.js"></script>
		<script src="../libs/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    	<script src="../libs/bootbox.min.js"></script>
		<script>
			function inPosition(view){
			var height = view.outerHeight();
			var top_of_object = view.position().top + height/2
			var top_of_window = $(window).scrollTop() + $(window).height();
			if(top_of_window > top_of_object-view.outerHeight()/2)
			return true;
			return false;
			}
			function checkAnimation(){

			$('.fadeIn').each( function(i){
			if( inPosition($(this))){
			$(this).animate({'opacity':'1'},800);
			}
			});
			$('.marginIn').each( function(i){
			if( inPosition($(this))){
			$(this).animate({ marginTop: '70px', opacity: 1 }, 800);
			}
			});

			$('.lateFadeIn').each( function(i){

			var view = $(this);
			if( inPosition($(this))){
			$(this).animate({'opacity':'1'},800);
			}

			});
			}

			function isEmail(email) {
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			return regex.test(email);
			}

			function prepareAnimation() {
			$('.marginIn').css("opacity",0.1);
			$('.marginIn').each(function(){$(this).css("marginTop", 150)});
			$('.lateFadeIn').css("opacity",0.1);
			$('.fadeIn').css("opacity",0.1);
			}

			$(document).ready(function() {

			$("#email,#password").keypress(function(e) {
			//13 maps to the enter key
			if (e.keyCode == 13) {
			$("#login-submit").click();
			}
			})

			$('input, textarea').placeholder();
			var timer;

			prepareAnimation();
			checkAnimation();
			$(window).scroll(function(){
			clearTimeout(timer);
			timer = setTimeout(checkAnimation, 200);
			});

			$("#login-submit").click(function(){
			email = $("#email").val();
			password = $("#password").val();

			valid = true;
			if (email == "") {
			$('#email-group').addClass('has-error');
			valid = false;
			}
			else{
			$('#email-group').removeClass('has-error');
			}

			if (password == ""){
			$('#password-group').addClass('has-error');
			valid = false;
			}
			else{
			$('#password-group').removeClass('has-error');
			}

			if(!valid) {
			$('.error-group').text('Please fill all the fields.');
			$('.error-group').css('opacity',1);
			return;
			} else {
			$('.error-group').text('');
			$('.error-group').css('opacity',0);
			}

			if(!isEmail(email)) {
			$('#email-group').addClass('has-error');
			$('.error-group').text('Please enter the correct email.');
			$('.error-group').css('opacity',1);
			return;
			}
			else {
			$('.error-group').text('');
			$('.error-group').css('opacity',0);
			}

			$('#login-submit').text('Loading..');
			$.ajax({
			url:'<?php echo Properties::$LOGIN_ACTION ?>
				',
				data:{
				'email':email,
				'password':password
				},
				success:function(result){

				$('#login-submit').text('Login');

				if(result == 1) {
				$('.error-group').text('');
				$('.error-group').css('opacity',0);

				window.location.href = "profile.php";
				}
				else {
				$('#email-group').addClass('has-error');
				$('.error-group').text(result);
				$('.error-group').css('opacity',1);
				}
				}
				});

				});

				});
		</script>
		<script>
			$("#go-to-learn-more").click(function() {
				$('html, body').animate({
					scrollTop : $("#video").offset().top
				}, 500);
				return false;
			});
			
			$("#show-video-btn").click(function() {
				var note = "<embed style=\"margin:10px\" width=\"700\" height=\"600\" src=\"http://www.youtube.com/embed/7UgVRjPmKsA?autoplay=1\">";
				bootbox.dialog({
					message: note
				}).find('.modal-dialog').css({"width":"760px"}).find(".modal-body").css({"padding":"20px"});
				
				return false;
			});
		</script>
		<script>
			function getNextAndPrevPage(){
				var top_of_window = $(window).scrollTop();
				var top_introduction = $("#introduction").position().top;
				var top_video = $("#video").position().top;
				var top_intro_rate = $("#intro_rate").position().top;
				var top_intro_verification = $("#intro_verification").position().top;
				var top_get_start = $("#get_start").position().top;
				
				var tol = 100;
				if(top_of_window + tol >= top_get_start)
					return ["#intro_verification","#get_start"];
				if(top_of_window + tol >= top_intro_verification)
					return ["#intro_rate","#get_start"];
				if(top_of_window + tol >= top_intro_rate)
					return ["#video","#intro_verification"];
				if(top_of_window + tol >= top_video)
					return ["#introduction","#intro_rate"];
				if(top_of_window + tol >= top_introduction)
					return ["#introduction","#video"];
			}
			
			$("#prev-page").click(function(){
				var prev = getNextAndPrevPage()[0];
				$('html, body').animate({
					scrollTop : $(prev).offset().top
				}, 500);
				return false;
			});
			
			$("#next-page").click(function(){
				var next = getNextAndPrevPage()[1];
				$('html, body').animate({
					scrollTop : $(next).offset().top
				}, 500);
				return false;
			});
			
		</script>
	</body>
</html>