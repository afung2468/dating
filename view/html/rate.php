<?php
    require_once __DIR__."/../../service/ContactService.class.php";
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../service/PictureService.class.php";
    require_once __DIR__."/../../utility/Utility.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";
	require_once __DIR__."/../../service/MessageService.class.php";

    try {

	    require "component/userAuth.php";
	    require "component/userProfilePic.php";

        $contactService = new ContactService();
        $mContact = $contactService->loadByUserId($mid);

        $pictureService = new PictureService();
        $mContact = $contactService->loadByUserId($me->id);
        $mMainPicLink = "../imgs/profile-default.png";
        if($mContact->mainPhotoId != null) {
        	$mMainPicLink = $pictureService->getSmallLink($mContact->mainPhotoId);
        }
	}
	catch(Exception $e) {
        Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$e->getMessage(),false);
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Velidate</title>
        <link href="../libs/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/main.css" rel="stylesheet">
        <link href="../css/components.css" rel="stylesheet">
        <link rel="stylesheet" href="../libs/jquery-scrollbar/jquery.mCustomScrollbar.css" />
    	<link href="../css/font-awesome.css" rel="stylesheet">
    	<link href="../css/bootstrap-social.css" rel="stylesheet">
    	<style>
    	.mCSB_inside > .mCSB_container {
			margin-right: 15px;
		}
    	</style>
    </head>

     <body>
        <?php require "component/analyticstracking.php"?>
        <div id="message-box" class="alert alert-info" style="display:none"></div>
        <div id="error-box" class="alert alert-danger" style="display:none"></div>
        <div class="site-wrapper">
            <div class="site-wrapper-inner">
                <?php require "component/navigation.php"; ?>
                <div class="main-content">
                    <div class="content-container" style="display:none;">
                        <div class="column-rating-list">
                        	<div class="rating-list-wrapper">
	                        	<div class="rating-list" >
	                        		<div id="ratees-ask-login" class="rating-list-group rating-list-ask-login" style="display:none;">
                                    	<div class="rating-group-msg">Login social network to rate more friends</div>
                                    	<div class="by-facebook"><button onclick="window.location.href='<?php echo Properties::$GET_FACEBOOK_USER_ACTION ?>'" type="button" class="btn btn-block btn-social btn-facebook form-component"><i class="fa fa fa-facebook"></i>Rate Facebook Friends</button></div>
                                    	<div class="by-gmail"><button onclick="window.location.href='<?php echo Properties::$GET_GMAIL_USER_ACTION ?>'" type="button" class="btn btn-block btn-social btn-google-plus form-component"><i class="fa fa fa-google-plus"></i>Rate Gmail Contacts</button></div>
                                    </div>
                                    <div id="rating-ratees-list">
	                                    
			                    	</div>
	                            </div>
                            </div>
                        </div>
                        <div class="column-rating-target">
                        	<div id="rating-page1" class="rating-target-main">
                        		<h2 class="rating-start-title">Stay Anonymous while rating</h2>
                        	</div>
                        	<div id="rating-page2" class="rating-target-main" style="display:none;">
                        		<h2 class="rating-target-title">Enjoy Rating Anonymously</h2>
                        		<div class="rating-target-sub-title">Check to verify if your friend has right information</div>
                        		<div class="rating-target-image"><img src="../imgs/profile-default.png"></div>
                        		<div id="verify-age-group">
	                        		<div id="title-verify-age" class="rating-item-title">Is <span class="he-or-she"></span> <span class="target-age"></span> years old?</div>
	                        		<div  id="btns-verify-age" class="rating-btns">
	                        			<a data-verify="1" href="#" class="button_ssm grey">Yes</a>
	                        			<a data-verify="0" href="#" class="button_ssm grey">No</a>
	                        			<a data-verify="-1" href="#" class="button_ssm grey">Don't know</a>
	                        		</div>
                        		</div>
	                        	<div id="verify-profession-group">
	                        		<div id="title-verify-profession" class="rating-item-title rate-body">Is <span class="he-or-she"></span> <span class="profession"></span> by profession?</div>
	                        		<div id="btns-verify-profession" class="rating-btns">
	                        			<a data-verify="1" href="#" class="button_ssm grey">Yes</a>
	                        			<a data-verify="0" href="#" class="button_ssm grey">No</a>
	                        			<a data-verify="-1" href="#" class="button_ssm grey">Don't know</a>
	                        		</div>
                        		</div>
                        		<div id="verify-picture-group">
	                        		<div id="title-verify-picture" class="rating-item-title rate-body">Does <span class="he-or-she"></span> look like this in real life?</div>
	                        		<div id="btns-verify-picture" class="rating-btns">
	                        			<a data-verify="1" href="#" class="button_ssm grey body-rate">Yes</a>
	                        			<a data-verify="0" href="#" class="button_ssm grey body-rate">No</a>
	                        			<a data-verify="-1" href="#" class="button_ssm grey body-rate">Don't know</a>
	                        		</div>
                        		</div>
                        		<div id="next1" class="rate-next-step">
	                        		<a href="#" class="button_m blue button_long">Next</a>
                        		</div>
                        		<div class="rate-note">Note:You can rate without hesitation, because your friend will only receive an anonymous notification</div>
                        	</div>
                        	<div id="rating-page3" class="rating-target-main" style="display:none;">
                        		<h2 class="rating-target-title">Enjoy Rating Anonymously</h2>
                        		<br>
                        		<div class="rating-target-image"><img src="../imgs/profile-default.png"></div>
                        		<div id="rate-face-group">
	                        		<div class="rating-item-title">Face</div>
	                        		<div id="btns-rate-face" class="rating-btns">
	                        			<a data-rate="1" href="#" class="button_ssm grey"></a>
	                        			<a data-rate="2" href="#" class="button_ss grey"></a>
	                        			<a data-rate="3" href="#" class="button_ssm grey"></a>
	                        			<a data-rate="4" href="#" class="button_ssm grey"></a>
	                        			<a data-rate="5" href="#" class="button_ssm grey"></a>
	                        			<a data-rate="6" href="#" class="button_ssm grey"></a>
	                        		</div>
                        		</div>
	                        	<div id="rate-body-group">
	                        		<div class="rating-item-title rate-body">Body</div>
	                        		<div id="btns-rate-body" class="rating-btns">
	                        			<a data-rate="1" href="#" class="button_ssm grey body-rate"></a>
	                        			<a data-rate="2" href="#" class="button_ssm grey body-rate"></a>
	                        			<a data-rate="3" href="#" class="button_ssm grey body-rate"></a>
	                        			<a data-rate="4" href="#" class="button_ssm grey body-rate"></a>
	                        			<a data-rate="5" href="#" class="button_ssm grey body-rate"></a>
	                        			<a data-rate="6" href="#" class="button_ssm grey body-rate"></a>
	                        		</div>
                        		</div>
                        		<div id="next2" class="rate-next-step">
	                        		<a href="#" class="button_m blue button_long">Next</a>
                        		</div>
                        		<div class="rate-note">Note:You can rate without hesitation, because your friend will only receive an anonymous notification</div>
                        	</div>
                        	<div  id="rating-page4" class="rating-target-main" style="display:none;">
                        		<h2 id="complete-rating-title" class="rating-target-title">Complete and send anonymous rating</h2>
                        		<!--
	                        	<div class="rating-item-title">
	                        		We need to send a notification to the target via <span class="his-or-her"></span> email<span class="email"></span>.
	                        		The email will contain the verification and rating provided by you but it is an anonymous message so <span class="he-or-she"></span> won't know the rating comes from you.
	                        	</div>
	                        	<div id="ratee-email-group">
	                        		<div class="rating-item-title" >Please <span class="provide-or-confirm"></span> <span class="his-or-her"></span> email address. <span class="if-not-correct" ></span></div>
	                        		<div id="ratee-email-wrapper" class="form-group">
		                                <input type="email" class="form-control" name="email" id="ratee-email" placeholder="Email">
		                            </div>
	                        	</div>
	                        	-->
	                        	
	                        	<div class="rating-item-title">
	                        		<span class="is-member"></span><br>
									Please <span class="provide-or-confirm"></span> <span class="his-or-her"></span> email address to send an anonymous notification.
	                        	</div>
	                        	<div id="ratee-email-group">
	                        		<div id="ratee-email-wrapper" class="form-group">
		                                <input type="email" class="form-control" name="email" id="ratee-email" placeholder="Email">
		                            </div>
	                        	</div>
	                        	<div id="btn-rating-complete" class="rate-next-step">
	                        		<a href="#" class="button_m blue button_long">Complete</a>
                        		</div>
                        	</div>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
        <?php require "component/footer.php"?>
    </body>
    <script src="../libs/jquery-1.11.1.min.js"></script>
    <script src="../libs/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
	<script src="../libs/jquery-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="//js.pusher.com/2.2/pusher.min.js"></script>
	<script>
    	var ratees = null;
    	var ratee_target = null;
    	var ratee_index = -1;
    	
    	function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
    	
    	function calculateAge(birthday) { // birthday is a date
		    var ageDifMs = Date.now() - new Date(birthday).getTime();
		    var ageDate = new Date(ageDifMs); // miliseconds from epoch
		    return Math.abs(ageDate.getUTCFullYear() - 1970);
		}
    	
    	function initRateeTarget(index) {
    		if(index >= ratees.ratees.length) {
        		$("#error-box").text("Invalid Target.");
            	$("#error-box").show();
                $("#error-box").fadeOut(1000);
    			return;
    		}
    		
    		
    		$(".rating-list-group .rating-background-wrapper").removeClass("rating-list-selected");
    		$(".rating-list-group[data-index=\""+index+"\"] .rating-background-wrapper").addClass("rating-list-selected");
    		
    		ratee_index = index;
    		ratee_target = ratees.ratees[index];
    		//clear results
    		$(".rating-btns a").removeClass("blue-selected");
    		$(".rating-btns a").addClass("grey");
    		
    		//init visibility and wording of itemss
    		if(ratee_target.gender == 1){
    			$(".he-or-she").text("he");
    			$(".his-or-her").text("his");
    			$("#btns-rate-face a[data-rate=1]").text("It's Okay");
    			$("#btns-rate-face a[data-rate=2]").text("Average");
    			$("#btns-rate-face a[data-rate=3]").text("Good Looking");
    			$("#btns-rate-face a[data-rate=4]").text("Hunk");
    			$("#btns-rate-face a[data-rate=5]").text("Jaw Dropping");
    			$("#btns-rate-face a[data-rate=6]").hide()
    			
    			$("#btns-rate-body a[data-rate=1]").text("Needs work");
    			$("#btns-rate-body a[data-rate=2]").text("Large but shapely");
    			$("#btns-rate-body a[data-rate=3]").text("Toned");
    			$("#btns-rate-body a[data-rate=4]").text("Fit/athletic");
    			$("#btns-rate-body a[data-rate=5]").text("Jaw Dropping");
    			$("#btns-rate-body a[data-rate=6]").hide()
    		} 
    		else if(ratee_target.gender == 2) {
    			$(".he-or-she").text("she");
    			$(".his-or-her").text("her");
    			$("#btns-rate-face a[data-rate=1]").text("It's Okay");
    			$("#btns-rate-face a[data-rate=2]").text("Average");
    			$("#btns-rate-face a[data-rate=3]").text("Good Looking");
    			$("#btns-rate-face a[data-rate=4]").text("Beautiful");
    			$("#btns-rate-face a[data-rate=5]").text("Stunning");
    			$("#btns-rate-face a[data-rate=6]").show()
    			$("#btns-rate-face a[data-rate=6]").text("Jaw Dropping");
    			
    			$("#btns-rate-body a[data-rate=1]").text("Needs work");
    			$("#btns-rate-body a[data-rate=2]").text("It's okay");
    			$("#btns-rate-body a[data-rate=3]").text("Average");
    			$("#btns-rate-body a[data-rate=4]").text("Fit/Athletic");
    			$("#btns-rate-body a[data-rate=5]").text("Sexy");
    			$("#btns-rate-body a[data-rate=6]").show()
    			$("#btns-rate-body a[data-rate=6]").text("Jaw Dropping ");
    		}
    		
    		if(ratee_target.photoLinkMedium == null || ratee_target.photoLinkMedium == "") {
    			$("#verify-picture-group").hide();
    			ratee_target.photoVerification = -1;
    			$(".rating-target-image img").attr("src","../imgs/profile-default.png");
    		}
    		else {
    			$("#verify-picture-group").show();
    			$(".rating-target-image img").attr("src",ratee_target.photoLinkMedium);
    			ratee_target.photoVerification = null;
    		}
    		
    		if(ratee_target.birthday == null || ratee_target.birthday == "") {
    			$("#verify-age-group").hide();
    			ratee_target.ageVerification = -1;
    		}
    		else {
    			$("#verify-age-group").show();
    			$(".target-age").text(calculateAge(ratee_target.birthday));
    			ratee_target.ageVerification = null;
    		}
    		
    		if(ratee_target.profession == null || ratee_target.profession == "") {
    			$("#verify-profession-group").hide();
    			ratee_target.professionVerification = -1;
    		}
    		else {
    			$("#verify-profession-group").show();
    			$(".profession").text(ratee_target.profession);
    			ratee_target.professionVerification = null;
    		}
    		
    		ratee_target.faceRating = null;
    		ratee_target.bodyRating = null;
    		
    		if(ratee_target.email == null || ratee_target.email == "") {
    			$("#ratee-email-wrapper input").val("");
    			$(".provide-or-confirm").text("provide");
    			//$(".if-bot-correct").text("If the email is not correct, please enter the correct email.");
    		} else {
    			$("#ratee-email-wrapper input").val(ratee_target.email);
    			$(".provide-or-confirm").text("confirm");
    			//$(".if-bot-correct").text("");
    		}
    		
    		if(ratee_target.isMember!=1) {
    			$(".is-member").text("Oops! It looks like the user is not in the system yet");
    		}
    		
    		$(".rating-target-main").hide();
    		$("#rating-page3").show();
    	}
    	
		function updateList() {
    		ratees.gmail_available = true //skip gmail ratees by now
    		
        	if(ratees.facebook_available == false || ratees.gmail_available == false){
        		$("#ratees-ask-login").show();
        		
            	if(ratees.facebook_available == false) {
            		$(".by-facebook").show();
            		$(".rating-group-msg").text("Please login to Google to rate more friends");
            	}
            	else {
            		$(".by-facebook").hide();
            	}
            	
            	if(ratees.gmail_available == false) {
            		$(".by-gmail").show();
            		$(".rating-group-msg").text("Please login Google to rate more friends");
            	}
            	else {
            		$(".by-gmail").hide();
            	}
            	
            	if(ratees.facebook_available == false && ratees.gmail_available == false) {
            		$(".rating-group-msg").text("Please login Facebook or Google to rate your friends");
            	}
        	}
        	else{
        		$("#ratees-ask-login").hide();
        	}
        	
			html = ""
			for(var i=0; i<ratees.ratees.length; i++) {
        		var image = (ratees.ratees[i].photoLinkMedium == null? "../imgs/profile-default.png":ratees.ratees[i].photoLinkSmall);
				html += '<div data-index="'+i+'" class="rating-list-group">'+
                    		'<div class="rating-background-wrapper" >'+
                    		'<div class="rating-pic">'+
                                '<div class="align-marker"></div><img src="'+image+'">'+
                            '</div>'+
                            '<div class="rating-name"><div>'+
                            	ratees.ratees[i].name + 
                            '</div></div>'+
                            '</div>'+
                    	'</div>';
			}
			
			$("#rating-ratees-list").html(html);
			$(".rating-list-group").click(function(){
				initRateeTarget($(this).attr("data-index"));
			});
			
			setTimeout(function(){
				$("#rating-ratees-list").mCustomScrollbar("scrollTo","bottom")
			},100);
			
		}
		
		function postRating(ratee_index, ratee_target, target_email) {
			data = {
				index: ratee_index,
				professionVerification: ratee_target.professionVerification,
				photoVerification: ratee_target.photoVerification,
				ageVerification: ratee_target.ageVerification,
				faceRating: ratee_target.faceRating,
				bodyRating: ratee_target.bodyRating,
				rateeEmail: target_email
			}
			
			$.ajax({
                type: "POST",
                url: "<?php echo Properties::$RATING_POST_RATING_ACTION ?>",
                data: data,
                success: function(data) {
                	ratees.ratees.splice(ratee_index, 1);
                	ratee_index = -1;
                	
    				$(".rating-target-main").hide();
	    			$("#rating-page1").show();
	    			$('.rating-start-title').text("Your rating has been submitted!");
					$("#btn-rating-complete a").text("Complete");
                    updateList();
                },
                error: function(xhr, status, error) {
            		$("#error-box").text(xhr.responseText);
                	$("#error-box").show();
                    $("#error-box").fadeOut(1000);
					$("#btn-rating-complete a").text("Complete");
				}
            });
		}
		
		function configRatingBtns() {
			// page2
			$("#btns-verify-age a").click(function() {
				ratee_target.ageVerification = parseInt($(this).attr("data-verify"));
				$("#btns-verify-age a").removeClass("blue-selected");
				$("#btns-verify-age a").addClass("grey");
    			$(this).removeClass("grey");
    			$(this).addClass("blue-selected");
    			return false;
			});
			$("#btns-verify-profession a").click(function() {
				ratee_target.professionVerification = parseInt($(this).attr("data-verify"));
				$("#btns-verify-profession a").removeClass("blue-selected");
				$("#btns-verify-profession a").addClass("grey");
    			$(this).removeClass("grey");
    			$(this).addClass("blue-selected");
    			return false;
			});
			$("#btns-verify-picture a").click(function() {
				ratee_target.photoVerification = parseInt($(this).attr("data-verify"));
				$("#btns-verify-picture a").removeClass("blue-selected");
				$("#btns-verify-picture a").addClass("grey");
    			$(this).removeClass("grey");
    			$(this).addClass("blue-selected");
    			return false;
			});
			
			$("#btns-rate-face a").click(function() {
				ratee_target.faceRating = parseInt($(this).attr("data-rate"));
				$("#btns-rate-face a").removeClass("blue-selected");
				$("#btns-rate-face a").addClass("grey");
    			$(this).removeClass("grey");
    			$(this).addClass("blue-selected");
    			return false;
			});
			
			$("#btns-rate-body a").click(function() {
				ratee_target.bodyRating = parseInt($(this).attr("data-rate"));
				$("#btns-rate-body a").removeClass("blue-selected");
				$("#btns-rate-body a").addClass("grey");
    			$(this).removeClass("grey");
    			$(this).addClass("blue-selected");
    			return false;
			});
			
			$("#next1 a").click(function(){
				if(ratee_target.photoVerification===null || ratee_target.photoVerification===undefined || ratee_target.photoVerification==="") {
            		$("#error-box").text("Please verify.");
                	$("#error-box").show();
                    $("#error-box").fadeOut(2000);
                    return false;
				}
				
				if(ratee_target.ageVerification===null || ratee_target.ageVerification===undefined || ratee_target.ageVerification==="") {
            		$("#error-box").text("Please verify.");
                	$("#error-box").show();
                    $("#error-box").fadeOut(2000);
                    return false;
				}
				
				if(ratee_target.professionVerification===null || ratee_target.professionVerification===undefined || ratee_target.professionVerification==="") {
            		$("#error-box").text("Please verify.");
                	$("#error-box").show();
                    $("#error-box").fadeOut(2000);
                    return false;
				}
				
    			$(".rating-target-main").hide();
	    		$("#rating-page4").show();
	    		return false;
				
			});
			
			$("#next2 a").click(function(){
				if(ratee_target.faceRating===null || ratee_target.faceRating===undefined || ratee_target.faceRating==="") {
            		$("#error-box").text("Please rate.");
                	$("#error-box").show();
                    $("#error-box").fadeOut(2000);
                    return false;
				}
				
				if(ratee_target.bodyRating===null || ratee_target.bodyRating===undefined || ratee_target.bodyRating==="") {
            		$("#error-box").text("Please rate.");
                	$("#error-box").show();
                    $("#error-box").fadeOut(2000);
                    return false;
				}
				
    			$(".rating-target-main").hide();
	    		$("#rating-page2").show();
	    		
	    		return false;
			});
			
			$("#btn-rating-complete a").click(function(){
				 if($("#btn-rating-complete a").text() != "Submitting...") {
					 var email = $("#ratee-email").val();
					 if(email == null) {
					 	$("#error-box").text("Please enter the email.");
	                	$("#error-box").show();
	                    $("#error-box").fadeOut(2000);
	                    return false;
					 }
					 
					 if(!isEmail(email)){
					 	$("#error-box").text("Please enter a valid email.");
	                	$("#error-box").show();
	                    $("#error-box").fadeOut(2000);
	                    return false;
					 }
					 
					 postRating(ratee_index,ratee_target, email)
					 $("#btn-rating-complete a").text("Submitting...");
				 }
			
				 return false;
			});
		}
		
		$(document).ready(function() {
			$("#message-box").show();
			$("#message-box").text("Loading...");
			$(".content-container").hide();
			
			$(".rating-list-wrapper").mCustomScrollbar({
			    theme:"rounded-dark"
			});
			
			configRatingBtns();
			
			$.ajax({
                type: "GET",
                url: "<?php echo Properties::$RATING_GET_RATEES_ACTION ?>",
                dataType: "json",
                success: function(data) {
                	ratees = data;
                	
					$(".content-container").show();
                    $("#message-box").fadeOut(1000);
                    
                    updateList();
                    
                    var ratee_index = window.location.hash.replace("#","");
                    if(ratee_index != "" && ratees.ratees.length != 0) {
                    	initRateeTarget(ratee_index);
                    }
                },
                error: function(xhr, status, error) {
            		$("#error-box").text(xhr.responseText);
                	$("#error-box").show();
                    $("#error-box").fadeOut(1000);
				}
            });
		});
	</script>
	<script>
		selectTab("#rate-tab");
	</script>
	
</html>