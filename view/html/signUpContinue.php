<?php 
    require_once __DIR__."/../../utility/Utility.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";
    require_once __DIR__."/../../service/ContactService.class.php";
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../dto/Contact.class.php";
    require_once __DIR__."/../../dto/User.class.php";

    //TODO: check email
	//echo "before getting parameters";
    $email = $_POST["email"];
    $password = $_POST["password"];
	$fbsignup = $_POST["fromFacebook"];
	
	if (!session_id()) {
		session_start();
	}

    $user = new User();

    $user->email = $email;
    $user->password = $password;

    $user->facebookVerified = 0;

    // check facebook
	$userData = isset($_SESSION['userdata']) ? $_SESSION['userdata']: null;
	$userPics = isset($_SESSION['userpics']) ? $_SESSION['userpics']: null;
	
	if ($userData != null) {
		$user->facebookVerified = 1;
		if (strlen($userData['email-address']) > 0) {
			$user->email = $userData['email-address'];
			$user->fbId = $userData['fbid'];

            #if the email is registered, forward the user to profile
            $accountService = new AccountService();
            $existingUser = $accountService->loadByFbId($user->fbId);

            if($existingUser != null) {
				$accountService->loginUser($existingUser);
				$_SESSION['userdata'] = null;
				
				// clear the userpics here
				// to fix the bug that when create fake profiles
				// it creates the pictures in the session
				// account->signUpUser also has this code
				if(isset($_SESSION['userpics'])){
					$_SESSION['userpics'] = null;
					unset($_SESSION['userpics']);
				}
				
                Utility::redirect(Properties::$PROFILE_PAGE);
            }

		}
		if (strlen($userData['name']) > 0) {
			$user->userName = $userData['name'];
		}
		if (strlen($userData['gender']) > 0) {
			if ($userData['gender'] == "male") {
				$user->gender = 1;
			} elseif ($userData['gender'] == "female") {
				$user->gender = 2;
			} else {$user->gender = -1;}		
		}
		if (strlen($userData['date-of-birth']) > 0) {
			$UserDOB = str_replace("/", "-", $userData['date-of-birth']);
			$bYR = substr($UserDOB, -4);
			$user->birthday = $bYR . "-" . substr($UserDOB, 0, 5);
		}	 
	}

	//$_SESSION['userdata'] = null;

    // check contact
    $contactService = new ContactService();
    $contact = $contactService->getContactByEmail($email);

    if($contact != null) {
        if($contact->name != null && $contact->name != "") {
            $user->userName = $contact->name;
        }

        if($contact->gender != null && $contact->gender != -1) {
            $user->gender = $contact->gender;
        }

        if($contact->birthday != null && $contact->birthday != "") {
            $user->birthday = $contact->birthday;
        }

        if($contact->profession != null && $contact->profession != "") {
            $user->profession = $contact->profession;
        }
		
		$user->mainPhotoId = $contact->mainPhotoId;
    }
    $_SESSION['sign-up-form']=$user; 
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Velidate</title>
        <link href="../libs/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/main2.css" rel="stylesheet">
        <link href="../css/components.css" rel="stylesheet">
    </head>

     <body>
        <?php require "component/analyticstracking.php"?>
        <div class="site-wrapper">
            <div class="site-wrapper-inner for-footer-header">
                 <div class="header">
                    <div class="header_container">
                        <a class="logo" href="<?php  echo Properties::$HOME_PAGE ?>"><img src="../imgs/logo_black.png" /></a>
                    </div>
                </div>
                <div class="form-container need-footer need-header">
                    <h2 class="title">Welcome! <?php  if ($user->firstName() != null)  echo $user->firstName(); ?></h2>
                    <form id="sign-up-continue-form" method="post">
                        <div class="signup-continue-wrapper">

                            <?php  if ($user->gender == null || $user->gender == -1) {?>
                            <div id="gender-group" class="form-group">
                                <select id="gender"  name="gender" class="form-control">
                                    <option value="-1">Gender</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                </select>
                            </div>
                            <?php  }?>

                            <?php  if ($user->seeking == null || $user->seeking == -1) {?>
                            <div id="interest-group" class="form-group">
                                <select id="interest"  name="interest" class="form-control">
                                    <option value="-1">Interested in</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                    <option value="3">Both</option>
                                </select>
                            </div>
                            <?php }?>

                            <?php if ($user->userName == null || $user->userName == "") {?>
                            <div  id="username-group" class="form-group">
                                <input type="text" class="form-control" name="username" id="username" placeholder="Name">
                            </div>
                            <?php }?>

                            <?php if ($user->country == null || $user->country == "") {?>
                            <div  id="country-group" class="form-group">
                                <select id="country" class="form-control" name="country">
                                    <option value="0">Country</option>
                                    <option value="Canada">Canada</option>
                                </select>
                            </div>
                            <?php }?>

                            <?php if ($user->city == null || $user->city == "") {?>
                            <div id="location-group" class="form-group" style="overflow:hidden">
                                <input type="text" class="form-control" name="city" id="city" placeholder="City">
                            </div>
                            <?php }?>

                            <?php if ($user->zipPostalCode == null || $user->zipPostalCode == "") {?>
                            <div  id="postal-code-group" class="form-group">
                                <input type="text" class="form-control" name="postalCode" id="postalCode" placeholder="Postal/Zip code (Optional)">
                            </div>
                            <?php }?>

                            <?php if ($user->birthday == null || $user->birthday == "") {?>
                            <div class="form_label">
                                <strong>Birthday:</strong>
                            </div>
                            <div  id="birthday-group" class="form-group" style="overflow:hidden">
                                <select id="date" class="form-control" name="birthDate" style="width:107px; float:left;">
                                   <option value="0">Date</option>
                                </select>
                                <select id="month" class="form-control" name="birthMonth" style="width:107px; float:left; margin-left:15px;">
                                    <option value="0">Month</option>
                                </select>
                                <select id="year" class="form-control" name="birthYear" style="width:107px; float:right;">
                                    <option value="0">Year</option>
                                </select>
                            </div>
                            <?php }?>

                            <?php if ($user->profession == null || $user->profession == "") {?>
                            <div class="form-group"  style="text-align: left; padding-left: 5px; margin-bottom:10px;">
                                <label style="font-weight:100;">
							      <input id="isStudent" type="checkbox" checked disabled="disabled"> Are you a student?
							    </label>
                            </div>
                            <div class="form-group" id="profession-group" style="overflow:hidden;height:0px;">
                                <div>
                                    <input type="text" name="profession" class="form-control" id="profession" placeholder="Profession" value="student">
                                </div>
                            </div>
                            <div class="student-group" style="overflow:hidden;">
                                <div class="form-group" id="major-group">
                                    <input type="text" name="major" class="form-control" id="major" placeholder="Major">
                                </div>
                                <div class="form-group" id="program-group">
                                    <select id="program" name="program" class="form-control">
                                        <option value="0">Level</option>
                                        <option value="2">Apprenticeship</option>
                                        <option value="3">Bachelors</option>
                                        <option value="4">Diploma</option>
                                        <option value="5">Doctorate PhD</option>
                                        <option value="6">Masters</option>
                                    </select>
                                </div>
                                <div class="form-group" id="student-email-group">
                                    <input type="text" name="studentEmail" class="form-control" id="studentEmail" placeholder="Student Email">
                                </div>
                            </div>
                            <?php }?>

                            <div class="form-group" id="ethnicity-group">
                                <select id="ethnicity" name="ethnicity" class="form-control">
                                    <option value="0">Your Ethnicity</option>
                                    <option value="Asian">Asian</option>
                                    <option value="Black">Black</option>
                                    <option value="Caucasian">Caucasian</option>
                                    <option value="Hispanic">Hispanic</option>
                                    <option value="Indian">Indian</option>
                                    <option value="Middle Eastern">Middle Eastern</option>
                                    <option value="Mixed">Mixed</option>
                                    <option value="Native American">Native American</option>
                                    <option value="Others">Others</option>
                                </select>
                            </div>
                            
                            <?php if ($user->religion == null || $user->religion == "") {?>
                            <div class="form-group" id="religion-group">
                                <select id="religion" name="religion" class="form-control">
                                    <option value="0">Your Religion</option>
                                    <option value="Muslim">Muslim</option>
                                    <option value="Catholic">Catholic</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Buddhist">Buddhist</option>
                                    <option value="Sikh">Sikh</option>
                                    <option value="Jewish">Jewish</option>
                                    <option value="Baptist">Baptist</option>
                                    <option value="Christian">Christian-other</option>
                                    <option value="Christian">Chinese traditional religion</option>
                                    <option value="Nonreligious /Agnostic/Atheist">Non-religious/Agnostic/Atheist</option>
                                    <option value="Others">Others</option>
                                </select>
                            </div>
                            <?php }?>
                            
                            <?php if ($user->intent == null || $user->intent == "") {?>
                            <div class="form-group" id="intent-group">
                                <select id="intent" name="intent" class="form-control">
                                    <option value="0">I am here to</option>
                                    <option value="Make new friends">Make new friends</option>
                                    <option value="Chat">Chat</option>
                                    <option value="Date">Date</option>
                                </select>
                            </div>
                            <?php }?>
                            
                            <div class="form-group" id="about-me-group">
                            	<textarea id="about-me" name="about-me" class="form-control" rows="6" placeholder="About Me"></textarea>
                            </div>
                            
                            <div class="error-group">
                                <strong></strong>
                            </div> 
                            <div class="form-group" >
                                <a id="sign-up-cont-btn" class="button button_m blue" href="#">Continue</a>
                            </div> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php require "component/footer.php"?>     
    </body>
    <script src="../libs/jquery-1.11.1.min.js"></script>
    <script src="../libs/placeholder.js"></script>
    <script src="../libs/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    <script>
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        $(document).ready(function() {
            $('input, textarea').placeholder();

            $('.error-group').css("opacity",0);

            $('#isStudent').change(function(){
                if($('#isStudent').is(':checked')) {
                    $('.student-group').animate({'height':'150px'},500);
                    $('#profession-group').animate({'height':'0px'},500, function(){
                    	$('#profession').val("student");
                    });
                } 
                else {
                    $('.student-group').animate({'height':'0px'},500);
                    $('#profession').val("");
                    $('#profession-group').animate({'height':'40px'},500);
                }
            });


            for(var i=2002; i>=1920; i--) {
                 $('#year')
                    .append($("<option></option>")
                    .attr("value",i)
                    .text(i));
            }

            for(var i=1; i<=12; i++) {
                 $('#month')
                    .append($("<option></option>")
                    .attr("value",i)
                    .text(i));
            }

            for(var i=1; i<=31; i++) {
                 $('#date')
                    .append($("<option></option>")
                    .attr("value",i)
                    .text(i));
            }

            $('#sign-up-cont-btn').click(function(){
                $('.form-group').removeClass('has-error');

                var fill_all = true;

                if($('#gender').length > 0 && $('#gender').val() == -1) {
                    fill_all = false;
                    $('#gender-group').addClass('has-error');
                }

                if($('#interest').length > 0 && $('#interest').val() == -1) {
                    fill_all = false;
                    $('#interest-group').addClass('has-error');
                }


                if($('#username').length > 0 && $('#username').val() == '') {
                    fill_all = false;
                    $('#username-group').addClass('has-error');
                }

                if($('#country').length > 0 && $('#country').val() == 0) {
                    fill_all = false;
                    $('#country-group').addClass('has-error');
                }

                if($('#city').length > 0 && $('#city').val() == 0) {
                    fill_all = false;
                    $('#location-group').addClass('has-error');
                }

                if($('#date').length > 0&& $('#date').val() == 0) {
                    fill_all = false;
                    $('#birthday-group').addClass('has-error');
                }

                if($('#month').length > 0&& $('#month').val() == 0) {
                    fill_all = false;
                    $('#birthday-group').addClass('has-error');
                }

                if($('#year').length > 0&& $('#year').val() == 0) {
                    fill_all = false;
                    $('#birthday-group').addClass('has-error');
                }

                
                if($('#profession').length > 0 && $('#profession').val() == 0) {
                    fill_all = false;
                    $('#profession-group').addClass('has-error');
                }

                if($('#profession').length > 0&& $('#isStudent').is(':checked') && $('#major').val()=='') {
                    fill_all = false;
                    $('#major-group').addClass('has-error');
                }

                if($('#profession').length > 0 && $('#isStudent').is(':checked') && $('#program').val()==0) {
                    fill_all = false;
                    $('#program-group').addClass('has-error');
                }

                if($('#profession').length > 0 && $('#isStudent').is(':checked') && $('#studentEmail').val()=='') {
                    fill_all = false;
                    $('#student-email-group').addClass('has-error');
                }

                if($('#ethnicity').val() == 0) {
                    fill_all = false;
                    $('#ethnicity-group').addClass('has-error');
                }

                if($('#religion').val() == 0) {
                    fill_all = false;
                    $('#religion-group').addClass('has-error');
                }
				
				if($('#intent').val() == 0) {
                    fill_all = false;
                    $('#intent-group').addClass('has-error');
                }
				
                if(!fill_all) {
                    $('.error-group').text('Please fill all the required fields.');
                    $('.error-group').css('opacity',1);
                    return false;
                }

                if($('#profession').length > 0 && $('#isStudent').is(':checked') && !isEmail($('#studentEmail').val())) {
                    $('#student-email-group').addClass('has-error');
                    $('.error-group').text('Please enter the correct student email.');
                    $('.error-group').css('opacity',1);
                    return false;
                }
                
                if($('#username').length > 0) {
                    if($('#username').val().search(/[^A-Za-z\s]+/) != -1) {
                        $('#username-group').addClass('has-error');
                        $('.error-group').text('Only charset a-z and A-Z is allowed in a name.');
                        $('.error-group').css('opacity',1);
                        return false;
                    }
                }

                $('#sign-up-continue-form').attr("action","<?php  echo Properties::$COMPLETE_SIGNUP_ACTION ?>");

                if($('#profession') != null && $('#profession').val() == 'student') {
                    $('#sign-up-cont-btn').text('Loading..');
                    $.ajax({
                        url:'<?php  echo Properties::$IS_STUDENT_EMAIL_VALID_ACTION ?>',
                        data:{
                            'email':$('#studentEmail').val()
                        },
                        success:function(result){
                            $('#sign-up-cont-btn').text('Continue');
                            $('#sign-up-continue-form').submit();
                        },
                        error: function(xhr, status, error) {
                            $('#sign-up-cont-btn').text('Continue');
	                        $('#student-email-group').addClass('has-error');
	                        $('.error-group').text(xhr.responseText);
	                        $('.error-group').css('opacity',1);
						}
                    });
                }
                else {
                    $('#sign-up-continue-form').submit();
                }

                return false;
                
            });
        });
    </script>
</html>