<?php
    require_once __DIR__."/../../utility/Properties.class.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Velidate</title>
        <link href="../libs/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/main2.css" rel="stylesheet">
        <link href="../css/components.css" rel="stylesheet">
        <link href="../css/font-awesome.css" rel="stylesheet">
        <link href="../css/bootstrap-social.css" rel="stylesheet">
    </head>

     <body>
        <?php require "component/analyticstracking.php"?>
        <div class="site-wrapper">
            <div class="site-wrapper-inner">
                 <div class="header">
                    <div class="header_container">
                        <a class="logo" href="<?php echo Properties::$HOME_PAGE ?>"><img src="../imgs/logo_black.png" /></a>
                    </div>
                </div>
                <div class="signup-container need-footer need-header">
                    <div class="signup-init-wrapper">
                        <div class="by-facebook"><button onclick="window.location.href='<?php echo Properties::$GET_FACEBOOK_USER_ACTION ?>'" type="button" style="padding-left:105px" class="btn btn-block btn-social btn-facebook form-component"><i class="fa fa fa-facebook"></i> Sign up with Facebook</button></div>
                        <div><hr></div>
                        <form id="sign-up-form" action="<?php echo Properties::$SIGNUP_CONTINUE_PAGE ?>" method="post">
                            <div class="description_label">
                                <strong>To ensure that all profiles are real, we require all users on Velidate.com to sign up through facebook. We will never post on your Facebook account without your permission.</strong>
                            </div>
                            <!--
                            <div id="email-group" class="form-group">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                            </div>
                            <div  id="password-group" class="form-group">
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                            </div>
                            <div  id="confirm-password-group" class="form-group">
                                <input type="password" class="form-control" name="confirmPassword" id="confirmPassword" placeholder="Confirm Password">
                            </div>
                            
                            <div class="error-group">
                                <strong></strong>
                            </div> 
                            <input id="fromFacebook" name="fromFacebook" value="0" type="hidden"/> 
                            <div class="form-group" >
                                <a id="sign-up-btn" class="button button_m blue" href="#">Signup</a>
                            </div>  
                            -->
                            <div class="form-group" >
                                <div>Already a member? <a  href="<?php echo Properties::$GET_FACEBOOK_USER_ACTION ?>">Login</a></div>
                            </div>  
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <?php require "component/footer.php"?>
    </body>
    <script src="../libs/jquery-1.11.1.min.js"></script>
    <script src="../libs/placeholder.js"></script>
    <script src="../libs/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    <!--
    <script>

		function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        $(document).ready(function() {
            $('input, textarea').placeholder();

            $('.error-group').css("opacity",0);
			document.getElementById("fromFacebook").value = "0";

            $('#sign-up-btn').click(function(){
                $('.form-group').removeClass('has-error');
                
                var fill_all = true;

                
                if($('#email').val() == 0) {
                    fill_all = false;
                    $('#email-group').addClass('has-error');
                }
                if($('#password').val() == '') {
                    fill_all = false;
                    $('#password-group').addClass('has-error');
                }

                if($('#confirmPassword').val() == '') {
                    fill_all = false;
                    $('#confirm-password-group').addClass('has-error');
                }
                
                if(fill_all == false) {
                    $('.error-group').text('Please fill all the required fields.');
                    $('.error-group').css('opacity',1);
                    return false;
                }

                if(isEmail($('#email').val()) == false) {
                    $('#email-group').addClass('has-error');
                    $('.error-group').text('Please enter the correct email.');
                    $('.error-group').css('opacity',1);
                    return false;
                }
               
                
                if($('#password').val().length < 8 || $('#password').val().length > 23) {
                    $('#password-group').addClass('has-error');
                    $('.error-group').text('The length of a password should be between 8 and 23.');
                    $('.error-group').css('opacity',1);
                    return false;
                }
                

                if($('#password').val() !== $('#confirmPassword').val()) {
                    $('#confirm-password-group').addClass('has-error');
                    $('.error-group').text('The confirmed password does not match the password.');
                    $('.error-group').css('opacity',1);
                    return false;
                }

                

                $('#sign-up-btn').text('Loading..');
                $.ajax({
                    url:"<?php echo Properties::$IS_EMAIL_UNIQUE_ACTION ?>",
                    data:{
                        'email':$('#email').val()
                    },
                    success:function(result){
                        $('#sign-up-btn').text('Signup');

                        if(result == 1)
                            //document.getElementById("fromFacebook").value = "0";
							$('#sign-up-form').submit();
                        else {
                            $('#email-group').addClass('has-error');
                            $('.error-group').text('This email has been registered.');
                            $('.error-group').css('opacity',1);
                        }
                    }                
                });

                return false;

            });
            
        });
    </script>
    -->
</html>