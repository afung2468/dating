<?php
    require_once __DIR__."/../../utility/Properties.class.php";
    $msg = $_GET["msg"];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Velidate</title>
        <link href="../libs/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/main2.css" rel="stylesheet">
        <link href="../css/components.css" rel="stylesheet">
    </head>

     <body>
        <?php require "component/analyticstracking.php"?>
        <div class="site-wrapper">
            <div class="site-wrapper-inner for-footer-header">
                 <div class="header">
                    <div class="header_container">
                        <a class="logo" href="<?php echo Properties::$HOME_PAGE ?>"><img src="../imgs/logo_black.png" /></a>
                    </div>
                </div>
                <div class="message-container need-footer need-header">
                    <div><img src="../imgs/heart_img.png" /></div>
                    <p class="msg"><?php echo $msg ?></p>
                </div>
            </div>
        </div>
        <?php require "component/footer.php"?>
    </body>
    <script src="../libs/jquery-1.11.1.min.js"></script>
    <script src="../libs/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
</html>