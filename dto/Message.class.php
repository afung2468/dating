<?php
/**
 * Object represents table 'message'
 *
 * @author: http://phpdao.com
 * @date: 2014-05-07 19:16
 */
class Message {

	var $id;
	var $content;
	var $conversationId;
	var $createTime;
	var $updateTime;
	var $direction;
	var $isRead;

	// not in db
	var $fromMe;
	var $targetName;
	var $targetContactId;
	var $targetCountUnread;
	var $targetPhoto;
	var $isFirstMessageInThisConversation;

}
?>