<?php

class Ratee {
	
	const REAL = 1;
	const FAKE = 0;
	const UNKNOWN = -1;
	
	var $fbId;
	var $email;
	var $name;
	var $gender;
	var $birthday;
	var $profession;
	var $major;
	var $photoLinkMedium;
	var $photoLinkSmall;
	var $photoId;
	var $source;
	var $rateBefore;
	var $contactId;
	var $isMember;
	var $userId;
	var $orderRating;
	// 1 is real, 0 is false, -1 is unknown
	var $professionVerification;
	var $photoVerification;
	var $ageVerification;
	// rating, 1 - 5 points for male, 1 - 6 for females
	var $faceRating;
	var $bodyRating;

	public function firstName() {
		$names = explode(" ", $this -> name);
		return $names[0];
	}

}
?>
