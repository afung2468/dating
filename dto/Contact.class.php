<?php

/**
 * Object represents table 'contact'
 *
 * @author: http://phpdao.com
 * @date: 2014-05-07 19:16
 */
class Contact {

	var $id;
	var $email;
	var $fbId;
	var $name;
	var $userId;
	var $birthday;
	var $ageVerified;
	var $ageNotVerified;
	var $profession;
	var $professionVerified;
	var $professionNotVerified;
	var $mainPhotoVerified;
	var $mainPhotoNotVerified;
	var $createTime;
	var $updateTime;
	var $sumRatingFace;
	var $sumRatingBody;
	var $numRatingFace;
	var $numRatingBody;
	var $numAllVerified;
	var $gender;
	var $mainPhotoId;

	public function isPictureVerified() {
		return ($this -> mainPhotoVerified != 0 && $this -> mainPhotoVerified > $this -> mainPhotoNotVerified * 2);
	}

	public function isAgeVerified() {
		return ($this -> ageVerified != 0 && $this -> ageVerified > $this -> ageNotVerified * 2);
	}

	public function isProfessionVerified() {
		return ($this -> professionVerified != 0 && $this -> professionVerified > $this -> professionNotVerified * 2);
	}

	public function verificationPercentage() {
		$verifiedSum = $this -> mainPhotoVerified + $this -> ageVerified + $this -> professionVerified;

		$notVerifiedSum = $this -> mainPhotoNotVerified + $this -> ageNotVerified + $this -> professionNotVerified;

		$sum = $notVerifiedSum + $verifiedSum;
		if ($sum == 0)
			return 0;
		else {
			return $verifiedSum / $sum * 80;
		}
	}

	public function firstName() {
		$names = explode(" ", $this -> name);
		return $names[0];
	}
	
	public function averageRating() {
		if($this->numRatingFace + $this->numRatingBody == 0)
			return "N/A"; // by default it's 6
		//$avgFace = round($this->sumRatingFace/$this->numRatingFace);
		//$avgBody = round($this->sumRatingBody/$this->numRatingBody);
		$avgRating = round(($this->sumRatingFace/$this->numRatingFace + $this->sumRatingBody/$this->numRatingBody)/2);
		
		if($this->gender == 1) {
			if($avgRating==1)
				return 6;
			else if($avgRating==2)
				return 7;
			else if($avgRating==3)
				return 8;
			else if($avgRating==4)
				return 9;
			else if($avgRating==5)
				return 10;
		}
		else if($this->gender == 2) {
			if($avgRating==1)
				return 6;
			else if($avgRating==2)
				return 7;
			else if($avgRating==3)
				return 7.5;
			else if($avgRating==4)
				return 8;
			else if($avgRating==5)
				return 9;
			else if($avgRating==6)
				return 10;
		}
		else 
			return -1;
		
	}

}
?>