<?php
/**
 * Object represents table 'user'
 *
 * @author: http://phpdao.com
 * @date: 2014-05-07 19:16
 */

class User {

	const PENDING = 1;
	const CONFIRMED = 0;
	const GENDER_MALE = 1;
	const GENDER_FEMAIL = 2;
	const GENDER_UNKNOWN = -1;

	const FACEBOOK_VISIBLE = 1;
	const FACEBOOK_INVISIBLE = 0;

	var $id;
	var $email;
	var $userName;
	var $passwordHash;
	var $salt;
	var $isStudent;
	var $studentEmail;
	var $institute;
	var $major;
	var $accountStatus;
	var $createDate;
	var $updateDate;
	var $emailNotificationMessages;
	var $emailNotificationMatches;
	var $gender = -1;
	var $seeking = -1;
	var $longitude;
	var $latitude;
	var $streetAddress;
	var $city;
	var $proviceState;
	var $country;
	var $zipPostalCode;
	var $ethnicity;
	var $religion;
	var $education;
	var $height;
	var $facebookVerified;
	var $studentEmailVerified;
	var $numCoins;
	var $profession;
	var $birthday;
	var $aboutMe;
	var $firstName;
	var $lastName;
	var $contactId;
	var $mainPhotoId;
	var $fbId;
	var $totalLoginTimes;
	var $lastLoginTime;
	var $openFacebookLink;
	var $remindersSent = 0;
	var $intent;

	// transient data not in database
	var $password;
	var $birthYear;
	var $birthMonth;
	var $birthDate;
	var $displayProfession;

	public function displayLevel() {
		if ($this -> level = 2)
			return "Apprenticeship";
		if ($this -> level = 3)
			return "Bachelors";
		if ($this -> level = 4)
			return "Diploma";
		if ($this -> level = 5)
			return "Doctorate PhD";
		if ($this -> level = 6)
			return "Masters";
		return null;
	}

	public function displayDegreeTitle() {
		if ($this -> level = 3)
			return "Bachelors";
		if ($this -> level = 5)
			return "PhD";
		if ($this -> level = 6)
			return "Masters";
		return "";
	}

	public function displayProfession() {
		if ($this -> profession == "student") {
			return $this -> major . " Student";
		}
		return $this -> profession;
	}

	public function firstName() {
		$names = explode(" ", $this -> userName);
		return $names[0];
	}

	public function getGender() {

		if ($this -> gender == 1) {

			return "Male";
		} else if ($this -> gender == 2) {

			return "Female";
		} else {

			return "Unknown";
		}

	}

	public function getSeeking() {

		if ($this -> seeking == 1) {

			return "Male";
		} else if ($this -> seeking == 2) {

			return "Female";
		} else if ($this -> seeking == 3) {

			return "Both";
		} else {

			return "Unknown";
		}

	}
	

}
?>