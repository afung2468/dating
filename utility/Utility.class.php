<?php

class Utility {
	public static function redirect($url, $permanent = false) {
		header('Location: ' . $url, true, $permanent ? 301 : 302);
		exit();
	}

	public static function message($code, $message) {
		http_response_code($code);
		echo $message;
		exit();
	}

}
?>