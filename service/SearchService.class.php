<?php
require_once __DIR__ . '/../dao/mysql/sql/Transaction.class.php';
require_once __DIR__ . '/../dao/mysql/sql/Connection.class.php';
require_once __DIR__ . '/../dao/mysql/sql/ConnectionFactory.class.php';
require_once __DIR__ . '/../dao/mysql/sql/ConnectionProperty.class.php';
require_once __DIR__ . '/../dao/mysql/sql/SqlQuery.class.php';
require_once __DIR__ . '/../dao/mysql/sql/QueryExecutor.class.php';
require_once __DIR__ . '/../service/PictureService.class.php';
require_once __DIR__ . '/../service/ContactService.class.php';

class SearchService {
	const NUM_PER_PAGE = 10;

	public function search($searchCriteria, $page) {
		$sql = "Select id, username as name, birthday, profession, contactId, mainPhotoId as pictureId From `user` " . $searchCriteria -> whereSQL() . " and accountStatus=0" . $searchCriteria -> orderBy() . " LIMIT " . ($page * SearchService::NUM_PER_PAGE) . ", " . SearchService::NUM_PER_PAGE;

		$sqlQuery = new SqlQuery($sql);
		$results = QueryExecutor::execute($sqlQuery);

		$pictureService = new PictureService();
		$contactService = new ContactService();
		for ($i = 0; $i < count($results); $i++) {
			if ($results[$i]["pictureId"] != null) {
				$results[$i]["picUrl"] = $pictureService -> getSmallLink($results[$i]["pictureId"]);
			}

			$contact = $contactService -> load($results[$i]["contactId"]);
			$results[$i]["imgCheck"] = $contact -> isPictureVerified();
			$results[$i]["professionCheck"] = $contact -> isProfessionVerified();
			$results[$i]["ageCheck"] = $contact -> isAgeVerified();
			$results[$i]["profession"] = htmlspecialchars($contact->profession);
			$results[$i]["name"] = htmlspecialchars($results[$i]["name"]);
			$fullNameArr = explode(" ", $results[$i]["name"]);
			$results[$i]["firstName"] = htmlspecialchars($fullNameArr[0]);
		}

		return json_encode($results);
	}

}
?>