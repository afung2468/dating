<?php
require_once __DIR__ . "/../dao/mysql/InstituteMySqlDAO.class.php";
require_once __DIR__ . "/../service/PictureService.class.php";
require_once __DIR__ . '/../utility/Properties.class.php';

class EmailService {
	static $header = "";

	public function sendEmail($subject, $targetAddress, $content) {

		if (empty($this -> header)) {
			$this -> header = 'MIME-Version: 1.0' . "\r\n";
			$this -> header .= 'Content-type: text/html; charset=ISO-8859-1' . "\r\n";
			$this -> header .= 'From: velidate <velidate@velidate.com>' . "\r\n";
			$this -> header .= 'Reply-To: velidate@velidate.com' . "\r\n";
			$this -> header .= "Return-Path: velidate@velidate.com" . "\r\n";
			$this -> header .= 'X-Mailer: Microsoft Office Outlook, Build 11.0.5510';
		}

		if (mail($targetAddress, $subject, $content, $this -> header)) {
			$ret = 0;
		} else {
			$ret = 1;
		}
		return $ret;
	}

	public function sendConfirmation($targetAddress, $link) {
		$emConfTemp = file_get_contents("http://" . $_SERVER['HTTP_HOST'] . "/service/email_templates/email_confirm_templ.html", true);
		$actualContents = str_replace("??confirm email??", $link, $emConfTemp);
		$retCode = $this -> sendEmail("Please confirm your email", $targetAddress, $actualContents);
		return $retCode;
	}

	public function sendStudentConfirmation($targetAddress, $link) {
		$emConfTemp = file_get_contents("http://" . $_SERVER['HTTP_HOST'] . "/service/email_templates/student_confirm_templ.html", true);
		$actualContents = str_replace("??confirm email??", $link, $emConfTemp);
		$retCode = $this -> sendEmail("Please confirm your student email", $targetAddress, $actualContents);
		return $retCode;
	}

	public function sendRatingEmail($rater, $ratee) {
		$emConfTemp = file_get_contents("http://" . $_SERVER['HTTP_HOST'] . "/service/email_templates/email_rated_templ.html", true);
		$photoservice = new PictureService;
		$photo = $photoservice -> getMainPhoto($rater -> contactId);
		$blurlink = $photoservice -> getBlurLink($photo -> id);
		$actualContents = str_replace("??sourcejpg??", $blurlink, $emConfTemp);
		$actualContents = str_replace("??rated response??", Properties::$DOMAIN, $actualContents);
		$actualContents = str_replace("??ratee name??", $ratee -> firstName(), $actualContents);
		$retCode = $this -> sendEmail("Hi, " . $ratee -> firstName() . ". You have been rated in Velidate.com", $ratee -> email, $actualContents);
		return $retCode;
	}
	
	public function sendMessageReminder($targetAddress, $nMessages) {
		$emRemindTemp = file_get_contents("email_templates/message_reminder_templ.html", true);
		$message = "1 new message";
		//$retCode = 0;
		if ($nMessages > 1) {
			$message = "$nMessages new messages";	
		}		
		$actualContents = str_replace("??messages??", $message, $emRemindTemp);
		$tsubject = "You have $message in Veledate";
		//echo "$tsubject  <br>";
		$retCode = $this -> sendEmail($tsubject, $targetAddress, $actualContents);
		//echo $actualContents;
		return $retCode;
	}

}
?>