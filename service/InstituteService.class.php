<?php
require_once __DIR__ . "/../dao/mysql/InstituteMySqlDAO.class.php";
require_once __DIR__ . '/../utility/Properties.class.php';

class InstituteService {

	public function getSchoolFromEmail($email){
		$dao = new InstituteMySqlDAO();
		$school = $dao -> findInstitute($email);
		if($school == null)
			return null;
		return $school->instituteName;
	}
	
	public function isStudentEmailValid($email) {
		// Just match the college name and ignore anyting after the last dot.
		//$atPos = strpos($email, "@");
		//$collegeDomain = substr($email, $atPos + 1);
		//$atPos = strrpos($collegeDomain, ".");
		//$college = substr($collegeDomain, 0, $atPos+1);
		//if (strlen($college) < 4) {return false;} // Make sure the college name is at least 3 chars
		//$colleges = new InstituteMySqlDAO();
		$school = $this->getSchoolFromEmail($email);
		//$size = count($colleges -> loadLike($college));
		if ($school == null) {
			return false;
		} else {
			return true;
		}
	}
}
?>