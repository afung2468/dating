<?php
require_once __DIR__ . "/../dao/mysql/PhotoMySqlDAO.class.php";
require_once __DIR__ . "/../dto/Photo.class.php";
require_once __DIR__ . "/../utility/Properties.class.php";
require_once __DIR__ . "/../dao/mysql/ContactMySqlDAO.class.php";
require_once __DIR__ . "/../dao/mysql/UserMySqlDAO.class.php";

class PictureService {

	private $iv = '6a71f09abd9ca8b6';
	private $key = 'a90e776f3d861b24';

	private $photoDAO;
	function __construct() {
		$this -> photoDAO = new PhotoMySqlDAO();
	}

	/**
	 * @param string $str
	 * @param bool $isBinary whether to encrypt as binary or not. Default is: false
	 * @return string Encrypted data
	 */
	function encrypt($pid, $size, $isBinary = false) {
		$str = $size . "&" . $pid;

		$iv = $this -> iv;
		$str = $isBinary ? $str : utf8_decode($str);

		$td = mcrypt_module_open('rijndael-128', ' ', 'cbc', $iv);

		mcrypt_generic_init($td, $this -> key, $iv);
		$encrypted = mcrypt_generic($td, $str);

		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);

		$code = $isBinary ? $encrypted : bin2hex($encrypted);
		return $code;
	}

	/**
	 * @param string $code
	 * @param bool $isBinary whether to decrypt as binary or not. Default is: false
	 * @return string Decrypted data
	 */
	function decrypt($code, $isBinary = false) {
		$code = $isBinary ? $code : $this -> hex2bin($code);
		$iv = $this -> iv;

		$td = mcrypt_module_open('rijndael-128', ' ', 'cbc', $iv);

		mcrypt_generic_init($td, $this -> key, $iv);
		$decrypted = mdecrypt_generic($td, $code);

		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);

		return $isBinary ? trim($decrypted) : utf8_encode(trim($decrypted));
	}

	protected function hex2bin($hexdata) {
		$bindata = '';

		for ($i = 0; $i < strlen($hexdata); $i += 2) {
			$bindata .= chr(hexdec(substr($hexdata, $i, 2)));
		}

		return $bindata;
	}

	public function getMediumLink($id) {
		return Properties::$PHOTO_GET_PHOTO_ACTION . "?code=" . $this -> encrypt($id, "m");
	}

	public function getOriginalLink($id) {
		return Properties::$PHOTO_GET_PHOTO_ACTION . "?code=" . $this -> encrypt($id, "o");
	}

	public function getSmallLink($id) {
		return Properties::$PHOTO_GET_PHOTO_ACTION . "?code=" . $this -> encrypt($id, "s");
	}

	public function getBlurLink($id) {
		return Properties::$PHOTO_GET_PHOTO_ACTION . "?code=" . $this -> encrypt($id, "ss");
	}

	public function getRealMediumLink($id) {
		return Properties::$PHOTO_PREX . "p" . intval($id / 1000) . "/" . $id . "_m.jpg";
	}

	public function getRealOriginalLink($id) {
		return Properties::$PHOTO_PREX . "p" . intval($id / 1000) . "/" . $id . "_o.jpg";
	}

	public function getRealSmallLink($id) {
		return Properties::$PHOTO_PREX . "p" . intval($id / 1000) . "/" . $id . "_s.jpg";
	}

	public function getRealBlurLink($id) {
		return Properties::$PHOTO_PREX . "p" . intval($id / 1000) . "/" . $id . "_ss.jpg";
	}

	public function getMainPhoto($contactId) {
		$p = $this -> photoDAO -> loadMainPhotoByContactId($contactId);
		if ($p == null)
			return null;

		$p -> linkMedium = $this -> getMediumLink($p -> id);
		$p -> linkOriginal = $this -> getOriginalLink($p -> id);
		$p -> linkSmall = $this -> getSmallLink($p -> id);
		$p -> linkBlur = $this -> getBlurLink($p -> id);
		return $p;
	}

	public function getUserPhotos($contactId) {
		$list = $this -> photoDAO -> loadByContactId($contactId);
		for ($i = 0; $i < count($list); $i++) {
			$list[$i] -> linkMedium = $this -> getMediumLink($list[$i] -> id);
			$list[$i] -> linkOriginal = $this -> getOriginalLink($list[$i] -> id);
			$list[$i] -> linkSmall = $this -> getSmallLink($list[$i] -> id);
			$list[$i] -> linkBlur = $this -> getBlurLink($list[$i] -> id);
		}
		return $list;
	}

	public function getUserPhotosNum($contactId) {
		$list = $this -> photoDAO -> loadByContactId($contactId);
		if ($list == null)
			return 0;
		return count($list);
	}

	public function convertImage($originalImage, $outputImage, $quality) {
		// jpg, png, gif or bmp?
		$exploded = explode('.', $originalImage);
		$ext = $exploded[count($exploded) - 1];

		if (preg_match('/jpg|jpeg/i', $ext))
			$imageTmp = imagecreatefromjpeg($originalImage);
		else if (preg_match('/png/i', $ext))
			$imageTmp = imagecreatefrompng($originalImage);
		else if (preg_match('/gif/i', $ext))
			$imageTmp = imagecreatefromgif($originalImage);
		else if (preg_match('/bmp/i', $ext))
			$imageTmp = imagecreatefrombmp($originalImage);
		else
			return 0;
		// quality is a value from 0 (worst) to 100 (best)
		imagejpeg($imageTmp, $outputImage, $quality);
		imagedestroy($imageTmp);
		return 1;
	}

	/**
	 * easy image resize function
	 * @param  $file - file name to resize
	 * @param  $width - new image width
	 * @param  $height - new image height
	 * @param  $proportional - keep image proportional, default is no
	 * @param  $output - name of the new file (include path if needed)
	 * @param  $delete_original - if true the original image will be deleted
	 * @param  $use_linux_commands - if set to true will use "rm" to delete the image, if false will use PHP unlink
	 * @param  $quality - enter 1-100 (100 is best quality) default is 100
	 * @return boolean|resource
	 */
	public function smart_resize_image($file, $width = 0, $height = 0, $proportional = false, $output = 'file', $delete_original = true, $use_linux_commands = false, $quality = 100) {

		if ($height <= 0 && $width <= 0)
			return false;

		# Setting defaults and meta
		$info = getimagesize($file);
		$image = '';
		$final_width = 0;
		$final_height = 0;
		list($width_old, $height_old) = $info;
		$cropHeight = $cropWidth = 0;

		# Calculating proportionality
		if ($proportional) {
			if ($width == 0)
				$factor = $height / $height_old;
			elseif ($height == 0)
				$factor = $width / $width_old;
			else
				$factor = min($width / $width_old, $height / $height_old);

			$final_width = round($width_old * $factor);
			$final_height = round($height_old * $factor);
		} else {
			$final_width = ($width <= 0) ? $width_old : $width;
			$final_height = ($height <= 0) ? $height_old : $height;
			$widthX = $width_old / $width;
			$heightX = $height_old / $height;

			$x = min($widthX, $heightX);
			$cropWidth = ($width_old - $width * $x) / 2;
			$cropHeight = ($height_old - $height * $x) / 2;
		}

		# Loading image to memory according to type
		switch ( $info[2] ) {
			case IMAGETYPE_JPEG :
				$image = imagecreatefromjpeg($file);
				break;
			case IMAGETYPE_GIF :
				$image = imagecreatefromgif($file);
				break;
			case IMAGETYPE_PNG :
				$image = imagecreatefrompng($file);
				break;
			default :
				return false;
		}

		# This is the resizing/resampling/transparency-preserving magic
		$image_resized = imagecreatetruecolor($final_width, $final_height);
		if (($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG)) {
			$transparency = imagecolortransparent($image);
			$palletsize = imagecolorstotal($image);

			if ($transparency >= 0 && $transparency < $palletsize) {
				$transparent_color = imagecolorsforindex($image, $transparency);
				$transparency = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
				imagefill($image_resized, 0, 0, $transparency);
				imagecolortransparent($image_resized, $transparency);
			} elseif ($info[2] == IMAGETYPE_PNG) {
				imagealphablending($image_resized, false);
				$color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
				imagefill($image_resized, 0, 0, $color);
				imagesavealpha($image_resized, true);
			}
		}
		imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);

		# Taking care of original, if needed
		if ($delete_original) {
			if ($use_linux_commands)
				exec('rm ' . $file);
			else
				@unlink($file);
		}

		# Preparing a method of providing result
		switch ( strtolower($output) ) {
			case 'browser' :
				$mime = image_type_to_mime_type($info[2]);
				header("Content-type: $mime");
				$output = NULL;
				break;
			case 'file' :
				$output = $file;
				break;
			case 'return' :
				return $image_resized;
				break;
			default :
				break;
		}

		# Writing image according to type to the output destination and image quality
		switch ( $info[2] ) {
			case IMAGETYPE_GIF :
				imagegif($image_resized, $output);
				break;
			case IMAGETYPE_JPEG :
				imagejpeg($image_resized, $output, $quality);
				break;
			case IMAGETYPE_PNG :
				$quality = 9 - (int)((0.9 * $quality) / 10.0);
				imagepng($image_resized, $output, $quality);
				break;
			default :
				return false;
		}

		return true;
	}// End of smart_resize_image

	public function putpic($pic, $imgFile) {
		// $pic is an object of Photo Class.
		// $imgFile contains the path and filename of the image
		// Convert photo to jpg, and store them in original, medium and small
		// Store in directory p0 for ID 1 to 999; p1 for ID 1000 to 1999 and so on.
		// File name is <ID>_o for original; _m for medium; _s for small.
		//echo "imgFile: $imgFile";
		list($width, $height) = getimagesize($imgFile);
		if ($width == 0) {
			throw new Exception("No photo uploaded or the uploaded file is not a picture.");
		}
		$ratio = $width / $height;
		if (($ratio < 0.55) or ($ratio > 1.78)) {
			throw new Exception("The aspect ratio of the photo is too tall or too wide.");
		}
		$pic -> createTime = round(microtime(true) * 1000);
		$pic -> updateTime = round(microtime(true) * 1000);
		$photoID = $this -> photoDAO -> insert($pic);
		// at this point, the original pic can be inserted
		$dirN = intval($photoID / 1000);
		//$dirPath = "http://".$_SERVER['HTTP_HOST']."/photos/p$dirN";
		//$dirPath = __DIR__ . "/../photos/p$dirN";
		$dirPath = Properties::$ROOT_PATH . Properties::$PHOTO_DIR."p$dirN";
		//echo "DirPath:  $dirPath";
		if (!is_dir($dirPath)) {// create the directory if it does not exist
			$succ = mkdir($dirPath);
			//echo "Creadted directory: $dirPath - $done";
		}
		$ofilePath = $dirPath . "/$photoID" . "_o.jpg";
		//echo "Image file:  $imgFile";
		$succ = $this -> convertImage($imgFile, $ofilePath, 90);
		if ($succ == 0) {
			throw new Exception("Original image cannot be stored.");
		}
		$result = unlink($imgFile);
		//echo "delete $imgFile : $result";
		$pic -> linkOriginal = $ofilePath;
		//$succ = strlen($filePath);
		//echo "Length of path: $succ <br>";
		$succ = $this -> photoDAO -> update($pic);
		// returns affected rows
		if ($succ == 0) {
			throw new Exception("Original image cannot be updated in DB.");
		}
		//echo "Saved: $filePath <br>";
		$done = true;
		$filePath = $dirPath . "/$photoID" . "_m.jpg";
		if ($width > $height) {
			if ($width > 600) {
				$done = $this -> smart_resize_image($ofilePath, 600, 0, true, $filePath, false, false, 90);
			} else {// if smaller than 600, just store a copy of original
				$done = copy($ofilePath, $filePath);
			}
		} else {
			if ($height > 600) {
				$done = $this -> smart_resize_image($ofilePath, 0, 600, true, $filePath, false, false, 90);
			} else {// if smaller than 600, just store a copy of original
				$done = copy($ofilePath, $filePath);
			}
		}
		if (!$done) {
			throw new Exception("Cannot resize to medium");
		}
		$pic -> linkMedium = $filePath;
		$succ = $this -> photoDAO -> update($pic);
		// returns affected rows
		if ($succ == 0) {
			throw new Exception("Medium image cannot be updated in DB.");
		}
		//echo "Saved: $filePath <br>";
		$filePath = $dirPath . "/$photoID" . "_s.jpg";
		if ($width > $height) {
			$done = $this -> smart_resize_image($ofilePath, 200, 0, true, $filePath, false, false, 90);
		} else {
			$done = $this -> smart_resize_image($ofilePath, 0, 200, true, $filePath, false, false, 90);
		}
		if (!$done) {
			throw new Exception("Cannot resize to small");
		}
		$pic -> linkSmall = $filePath;
		$succ = $this -> photoDAO -> update($pic);
		//echo "Saved: $filePath <br>";
		$filePath = $dirPath . "/$photoID" . "_ss.jpg";
		if ($width > $height) {
			$done = $this -> smart_resize_image($ofilePath, 5, 0, true, $filePath, false, false, 90);
		} else {
			$done = $this -> smart_resize_image($ofilePath, 0, 5, true, $filePath, false, false, 90);
		}
		if (!$done) {
			throw new Exception("Cannot resize to super small");
		}
		// returns affected rows
		if ($succ == 0) {
			throw new Exception("Small image cannot be updated in DB.");
		}
		return $photoID;
	}

	public function changeMainPhoto($contID, $photoID) {
		// fail if the photoId does not belong to the contact of contactId
		// set the isMianPhoto attribute of the main photo of the user as 0
		// set the isMianPhoto attribute of the photo of the photoId as 1.
		// update the database.
		$photoRow = $this -> photoDAO -> load($photoID);
		if ($photoRow == null) {
			throw new Exception("Photo cannot be found.");
		}
		if ($photoRow -> contactId != $contID) {
			throw new Exception("Photo does not belong to this contact.");
		}
		$photoRow -> isMainPhoto = 1;
		$succ = $this -> photoDAO -> update($photoRow);
		// returns affected rows
		if ($succ == 0) {
			throw new Exception("Failed to make this main photo.");
		} else {
			$contactDAO = new ContactMysqlDAO();
			$userDAO = new UserMySqlDAO();

			$contact = $contactDAO -> load($contID);
			$contact -> mainPhotoId = $photoID;
			$contact -> mainPhotoVerified = 0;
			$contact -> mainPhotoNotVerified = 0;
			$contactDAO -> update($contact);

			$user = $userDAO -> load($contact -> userId);
			$user -> mainPhotoId = $photoID;
			$userDAO -> update($user);
		}
		$photoList = $this -> photoDAO -> loadByContactId($contID);
		//echo "Number of photo row for this contact ID: " . count($photoList);
		for ($i = 0; $i < count($photoList); $i++) {
			//var_dump($photoList[$i]);
			if (($photoList[$i] -> id != $photoID) && ($photoList[$i] -> isMainPhoto == 1)) {
				$photoList[$i] -> isMainPhoto = 0;
				$succ = $this -> photoDAO -> update($photoList[$i]);
				return;
			}
		}
		return;
	}

	public function changeVisibility($photoID, $visibility) {
		// Change the visibility of the photo of $photoID to $visibility.
		$photoRow = $this -> photoDAO -> load($photoID);
		$photoRow -> visibility = $visibility;

		$succ = $this -> photoDAO -> update($photoRow);
		// returns affected rows
		if ($succ == 0) {
			throw new Exception("Failed to change visibility of this photo.");
		}
		return;
	}

	public function loadPhotoFromUrl($url, $contID) {
		//$fType = substr($url, -4);
		$dot = strrpos($url, '.');
		$fType = substr($url, $dot + 1);
		if (preg_match('/^([a-z]+)/i', $fType, $matches)) {
			$fType = '.' . $matches[0];
		} else {
			$fType = ".jpg";
			//assumed file type
		}
		//$filePath = __DIR__ . "/../photos/temp" . $contID . $fType . "<br>";
		//$filePath = Properties::$PHOTO_PREX . "temp" . $contID . $fType . "<br>";
		$filePath = Properties::$ROOT_PATH . Properties::$PHOTO_DIR."temp" . $contID . $fType . "<br>";
		//echo "Temp file path: " . $filePath;
		if (!copy($url, $filePath)) {
			throw new Exception("Failed to get file from URL.");
		}
		chmod($filePath, 0666);
		$thisphoto = new Photo;
		$thisphoto -> verified = 0;
		$thisphoto -> notVerified = 0;
		$thisphoto -> linkOriginal = "No link";
		$thisphoto -> linkLarge = "No link";
		$thisphoto -> linkMedium = "No link";
		$thisphoto -> linkSmall = "No link";
		$thisphoto -> visibility = 0;
		$thisphoto -> isMainPhoto = 0;
		$thisphoto -> contactId = $contID;
		$photoID = $this -> putpic($thisphoto, $filePath);
		unlink($filePath);
		return $photoID;
	}

	public function loadPhotoFromUrl2($url, $thisphoto) {
		//$fType = substr($url, -4);
		$dot = strrpos($url, '.');
		$fType = substr($url, $dot + 1);
		if (preg_match('/^([a-z]+)/i', $fType, $matches)) {
			$fType = '.' . $matches[0];
		} else {
			$fType = ".jpg";
			//assumed file type
		}
		//$filePath = __DIR__ . "/../photos/temp" . $thisphoto -> contactId . $fType . "<br>";
		//$filePath = Properties::$PHOTO_PREX . "temp" . $thisphoto -> contactId . $fType . "<br>";
		$filePath = Properties::$ROOT_PATH . Properties::$PHOTO_DIR."temp" . $thisphoto -> contactId . $fType . "<br>";
		//echo "Temp file path: " . $filePath;
		if (!copy($url, $filePath)) {
			throw new Exception("Failed to get file from URL.");
		}
		chmod($filePath, 0666);
		$thisphoto -> photoID = $this -> putpic($thisphoto, $filePath);
		unlink($filePath);
		return $thisphoto;
	}

	public function load($pid) {
		return $this -> photoDAO -> load($pid);
	}

	public function delete($pid) {
		return $this -> photoDAO -> delete($pid);
	}

	public function update($photo) {
		$this -> photoDAO -> update($photo);
	}

}
?>