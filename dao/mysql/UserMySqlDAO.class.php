<?php

require_once 'sql/SqlQuery.class.php';
require_once 'sql/QueryExecutor.class.php';
require_once 'sql/Transaction.class.php';
require_once 'sql/Connection.class.php';
require_once 'sql/ConnectionFactory.class.php';
require_once 'sql/ConnectionProperty.class.php';
require_once __DIR__.'/../../dto/User.class.php';
/*
 * Class that operate on table 'user'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2014-05-07 19:16
 */
class UserMySqlDAO{

	/**
	 * Get Domain object by email
	 *
	 * @param String $email
	 * @Return User list with the email
	 */
	public function loadByEmail($email){
		$sql = 'SELECT * FROM user WHERE email = ? LIMIT 1';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($email);
		return $this->getRow($sqlQuery);
	}
	
	public function loadByFbId($fbId){
		$sql = 'SELECT * FROM user WHERE fbId = ? LIMIT 1';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($fbId);
		return $this->getRow($sqlQuery);
	}
	
	public function loadByContactId($contactId){
		$sql = 'SELECT * FROM user WHERE contactId = ? LIMIT 1';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($contactId);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get Domain object by student email
	 *
	 * @param String $email
	 * @Return User list with the email
	 */
	public function loadByStudentEmail($email){
		$sql = 'SELECT * FROM user WHERE studentEmail = ? LIMIT 1';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($email);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return UserMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM user WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->getRow($sqlQuery);
	}
	
	/**
	 * Get id, email, remindersSent from table where remindersSent < 3
	 */
	public function queryWithRemindersLT3(){
		$sql = 'SELECT id, email, remindersSent FROM user where remindersSent < 3';
		$sqlQuery = new SqlQuery($sql);
		return QueryExecutor::executeForResult($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM user';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM user ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param user primary key
 	 */
	public function delete($id){
		$sql = 'DELETE FROM user WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param UserMySql user
 	 */
	public function insert($user){
		$sql = 'INSERT INTO user (email, userName, passwordHash, salt, isStudent, studentEmail, institute, major, accountStatus, createDate, updateDate, emailNotificationMessages, emailNotificationMatches, gender, seeking, longitude, latitude, streetAddress, city, proviceState, country, zipPostalCode, ethnicity, religion, education, height, facebookVerified, studentEmailVerified, numCoins, profession, birthday, aboutMe, firstName, lastName, contactId, mainPhotoId, fbId, totalLoginTimes, lastLoginTime, openFacebookLink, remindersSent, intent) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($user->email);
		$sqlQuery->set($user->userName);
		$sqlQuery->set($user->passwordHash);
		$sqlQuery->set($user->salt);
		$sqlQuery->setNumber($user->isStudent);
		$sqlQuery->set($user->studentEmail);
		$sqlQuery->set($user->institute);
		$sqlQuery->set($user->major);
		$sqlQuery->setNumber($user->accountStatus);
		$sqlQuery->set($user->createDate);
		$sqlQuery->set($user->updateDate);
		$sqlQuery->setNumber($user->emailNotificationMessages);
		$sqlQuery->setNumber($user->emailNotificationMatches);
		$sqlQuery->setNumber($user->gender);
		$sqlQuery->setNumber($user->seeking);
		$sqlQuery->set($user->longitude);
		$sqlQuery->set($user->latitude);
		$sqlQuery->set($user->streetAddress);
		$sqlQuery->set($user->city);
		$sqlQuery->set($user->proviceState);
		$sqlQuery->set($user->country);
		$sqlQuery->set($user->zipPostalCode);
		$sqlQuery->set($user->ethnicity);
		$sqlQuery->set($user->religion);
		$sqlQuery->set($user->education);
		$sqlQuery->set($user->height);
		$sqlQuery->setNumber($user->facebookVerified);
		$sqlQuery->setNumber($user->studentEmailVerified);
		$sqlQuery->setNumber($user->numCoins);
		$sqlQuery->set($user->profession);
		$sqlQuery->set($user->birthday);
		$sqlQuery->set($user->aboutMe);
		$sqlQuery->set($user->firstName);
		$sqlQuery->set($user->lastName);
		$sqlQuery->set($user->contactId);
		$sqlQuery->setNumber($user->mainPhotoId);
		$sqlQuery->setNumber($user->fbId);
		$sqlQuery->setNumber($user->totalLoginTimes);
		$sqlQuery->set($user->lastLoginTime);
		$sqlQuery->setNumber($user->openFacebookLink);
		$sqlQuery->setNumber($user->remindersSent);
		$sqlQuery->set($user->intent);

		$id = $this->executeInsert($sqlQuery);	
		$user->id = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param UserMySql user
 	 */
	public function update($user){
		$sql = 'UPDATE user SET email = ?, userName = ?, passwordHash = ?, salt = ?, isStudent = ?, studentEmail = ?, institute = ?, major = ?, accountStatus = ?, createDate = ?, updateDate = ?, emailNotificationMessages = ?, emailNotificationMatches = ?, gender = ?, seeking = ?, longitude = ?, latitude = ?, streetAddress = ?, city = ?, proviceState = ?, country = ?, zipPostalCode = ?, ethnicity = ?, religion = ?, education = ?, height = ?, facebookVerified = ?, studentEmailVerified = ?, numCoins = ?, profession = ?, birthday = ?, aboutMe = ?, firstName = ?, lastName = ?, contactId = ?, mainPhotoId = ?, fbId = ?, totalLoginTimes = ?, lastLoginTime = ?, openFacebookLink = ?, remindersSent = ?, intent = ? WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($user->email);
		$sqlQuery->set($user->userName);
		$sqlQuery->set($user->passwordHash);
		$sqlQuery->set($user->salt);
		$sqlQuery->setNumber($user->isStudent);
		$sqlQuery->set($user->studentEmail);
		$sqlQuery->set($user->institute);
		$sqlQuery->set($user->major);
		$sqlQuery->setNumber($user->accountStatus);
		$sqlQuery->set($user->createDate);
		$sqlQuery->set($user->updateDate);
		$sqlQuery->setNumber($user->emailNotificationMessages);
		$sqlQuery->setNumber($user->emailNotificationMatches);
		$sqlQuery->setNumber($user->gender);
		$sqlQuery->setNumber($user->seeking);
		$sqlQuery->set($user->longitude);
		$sqlQuery->set($user->latitude);
		$sqlQuery->set($user->streetAddress);
		$sqlQuery->set($user->city);
		$sqlQuery->set($user->proviceState);
		$sqlQuery->set($user->country);
		$sqlQuery->set($user->zipPostalCode);
		$sqlQuery->set($user->ethnicity);
		$sqlQuery->set($user->religion);
		$sqlQuery->set($user->education);
		$sqlQuery->set($user->height);
		$sqlQuery->setNumber($user->facebookVerified);
		$sqlQuery->setNumber($user->studentEmailVerified);
		$sqlQuery->setNumber($user->numCoins);
		$sqlQuery->set($user->profession);
		$sqlQuery->set($user->birthday);
		$sqlQuery->set($user->aboutMe);
		$sqlQuery->set($user->firstName);
		$sqlQuery->set($user->lastName);
		$sqlQuery->set($user->contactId);
		$sqlQuery->setNumber($user->mainPhotoId);
		$sqlQuery->setNumber($user->fbId);
		$sqlQuery->setNumber($user->totalLoginTimes);
		$sqlQuery->set($user->lastLoginTime);
		$sqlQuery->setNumber($user->openFacebookLink);
		$sqlQuery->setNumber($user->remindersSent);
		$sqlQuery->set($user->intent);

		$sqlQuery->setNumber($user->id);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'TRUNCATE TABLE user';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
	 * Read row
	 *
	 * @return UserMySql 
	 */
	protected function readRow($row){
		$user = new User();
		
		$user->id = $row['id'];
		$user->email = $row['email'];
		$user->userName = $row['userName'];
		$user->passwordHash = $row['passwordHash'];
		$user->salt = $row['salt'];
		$user->isStudent = $row['isStudent'];
		$user->studentEmail = $row['studentEmail'];
		$user->institute = $row['institute'];
		$user->major = $row['major'];
		$user->accountStatus = $row['accountStatus'];
		$user->createDate = $row['createDate'];
		$user->updateDate = $row['updateDate'];
		$user->emailNotificationMessages = $row['emailNotificationMessages'];
		$user->emailNotificationMatches = $row['emailNotificationMatches'];
		$user->gender = $row['gender'];
		$user->seeking = $row['seeking'];
		$user->longitude = $row['longitude'];
		$user->latitude = $row['latitude'];
		$user->streetAddress = $row['streetAddress'];
		$user->city = $row['city'];
		$user->proviceState = $row['proviceState'];
		$user->country = $row['country'];
		$user->zipPostalCode = $row['zipPostalCode'];
		$user->ethnicity = $row['ethnicity'];
		$user->religion = $row['religion'];
		$user->education = $row['education'];
		$user->height = $row['height'];
		$user->facebookVerified = $row['facebookVerified'];
		$user->studentEmailVerified = $row['studentEmailVerified'];
		$user->numCoins = $row['numCoins'];
		$user->profession = $row['profession'];
		$user->birthday = $row['birthday'];
		$user->aboutMe = $row['aboutMe'];
		$user->firstName = $row['firstName'];
		$user->lastName = $row['lastName'];
		$user->contactId = $row['contactId'];
		$user->mainPhotoId = $row['mainPhotoId'];
		$user->fbId = $row['fbId'];
		$user->totalLoginTimes = $row['totalLoginTimes'];
		$user->lastLoginTime = $row['lastLoginTime'];
		$user->openFacebookLink = $row['openFacebookLink'];
		$user->remindersSent = $row['remindersSent'];
		$user->intent = $row['intent'];

		return $user;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return UserMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab) == 0) {
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>