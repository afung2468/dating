<?php
require_once 'sql/SqlQuery.class.php';
require_once 'sql/QueryExecutor.class.php';
require_once 'sql/Transaction.class.php';
require_once 'sql/Connection.class.php';
require_once 'sql/ConnectionFactory.class.php';
require_once 'sql/ConnectionProperty.class.php';
require_once __DIR__.'/../../dto/Message.class.php';

/*
 * Class that operate on table 'message'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2014-05-07 19:16
 */
class MessageMySqlDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return MessageMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM message WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->getRow($sqlQuery);
	}
	
	/**
	 * Get Domain object by conversation id
	 *
	 * @param String $conid, $startCreateTime, $length
	 * @return array of message 
	 */
	public function loadByConversationId($conid, $startCreateTime, $length){
		$sql = "SELECT * FROM message WHERE conversationId = ? AND createTime < ? ORDER BY createTime desc LIMIT 0, $length";
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($conid);
		$sqlQuery->set($startCreateTime);	
		return $this->getList($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM message';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM message ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param message primary key
 	 */
	public function delete($id){
		$sql = 'DELETE FROM message WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param MessageMySql message
 	 */
	public function insert($message){
		$sql = 'INSERT INTO message (content, conversationId, createTime, updateTime, direction, isRead, fromId, toId) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($message->content);
		$sqlQuery->setNumber($message->conversationId);
		$sqlQuery->set($message->createTime);
		$sqlQuery->set($message->updateTime);
		$sqlQuery->setNumber($message->direction);
		$sqlQuery->setNumber($message->isRead);
		
		$sqlQuery->setNumber($message->fromId);
		$sqlQuery->setNumber($message->toId);

		$id = $this->executeInsert($sqlQuery);	
		$message->id = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param MessageMySql message
 	 */
	public function update($message){
		$sql = 'UPDATE message SET content = ?, conversationId = ?, createTime = ?, updateTime = ?, direction = ? , isRead = ?, fromId = ?, toId = ? WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($message->content);
		$sqlQuery->setNumber($message->conversationId);
		$sqlQuery->set($message->createTime);
		$sqlQuery->set($message->updateTime);
		$sqlQuery->setNumber($message->direction);
		$sqlQuery->setNumber($message->isRead);
		
		$sqlQuery->setNumber($message->fromId);
		$sqlQuery->setNumber($message->toId);

		$sqlQuery->setNumber($message->id);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'TRUNCATE TABLE message';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
	 * Read row
	 *
	 * @return MessageMySql 
	 */
	protected function readRow($row){
		$message = new Message();
		
		$message->id = $row['id'];
		$message->content = $row['content'];
		$message->conversationId = $row['conversationId'];
		$message->createTime = $row['createTime'];
		$message->updateTime = $row['updateTime'];
		$message->direction = $row['direction'];
		$message->isRead = $row['isRead'];
		$message->fromId = $row['fromId'];
		$message->toId = $row['toId'];

		return $message;
	}
	
	public function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	public function getNumOfAllUnreadMessages($recieverId) {
		$sqlString = "SELECT count(*) FROM message m WHERE toId = '" . $recieverId . "' AND m.isRead='0'";
		$thisqry = new SqlQuery($sqlString);
		$thisqry -> params = null;
		$thisqry -> idx = 0;
		$topics = QueryExecutor::queryForString($thisqry);
		$result = mysql_result($topics, 0);

		return $result;
	}
	
	public function getNumOfUnreadMessages($conversationId, $recieverId) {
		$sqlString = "SELECT count(*) FROM message m WHERE m.conversationId = '" . $conversationId . "' And toId = '" . $recieverId . "' AND m.isRead='0'";
		$thisqry = new SqlQuery($sqlString);
		$thisqry -> params = null;
		$thisqry -> idx = 0;
		$topics = QueryExecutor::queryForString($thisqry);
		return mysql_result($topics, 0);
	}
	
	public function readAllMessagesOfConversation($conversationId, $receiverId) {
		$sqlString = "update message set isRead=1 where conversationId='" . $conversationId . "' and toId='" . $receiverId . "' and isRead=0";
		$thisqry = new SqlQuery($sqlString);
		$thisqry -> params = null;
		$thisqry -> idx = 0;
		$affectedRows = QueryExecutor::executeUpdate($thisqry);

		return $affectedRows;
	}
	
	public function getNumOfMessages($from, $to) {
		$sqlString = "SELECT count(*) FROM message m WHERE m.conversationId = '" . $conversationId . "' And toId = '" . $recieverId . "' AND m.isRead='0'";
		$thisqry = new SqlQuery($sqlString);
		$thisqry -> params = null;
		$thisqry -> idx = 0;
		$topics = QueryExecutor::queryForString($thisqry);
		return mysql_result($topics, 0);
	}
	
	/**
	 * Get row
	 *
	 * @return MessageMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab) == 0) {
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	public function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>