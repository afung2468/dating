<?php
require_once 'sql/SqlQuery.class.php';
require_once 'sql/QueryExecutor.class.php';
require_once 'sql/Transaction.class.php';
require_once 'sql/Connection.class.php';
require_once 'sql/ConnectionFactory.class.php';
require_once 'sql/ConnectionProperty.class.php';
require_once __DIR__.'/../../dto/Country.class.php';

/*
 * Class that operate on table 'country'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2014-05-07 19:16
 */
class CountryMySqlDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return CountryMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM country WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM country';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM country ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param country primary key
 	 */
	public function delete($id){
		$sql = 'DELETE FROM country WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param CountryMySql country
 	 */
	public function insert($country){
		$sql = 'INSERT INTO country (name, countryCode) VALUES (?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($country->name);
		$sqlQuery->setNumber($country->countryCode);

		$id = $this->executeInsert($sqlQuery);	
		$country->id = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param CountryMySql country
 	 */
	public function update($country){
		$sql = 'UPDATE country SET name = ?, countryCode = ? WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($country->name);
		$sqlQuery->setNumber($country->countryCode);

		$sqlQuery->setNumber($country->id);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'TRUNCATE TABLE country';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
	 * Read row
	 *
	 * @return CountryMySql 
	 */
	protected function readRow($row){
		$country = new Country();
		
		$country->id = $row['id'];
		$country->countryCode = $row['countryCode'];
		$country->name = $row['name'];

		return $country;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return CountryMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab) == 0) {
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>