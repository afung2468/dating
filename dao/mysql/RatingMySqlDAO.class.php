<?php

require_once 'sql/SqlQuery.class.php';
require_once 'sql/QueryExecutor.class.php';
require_once 'sql/Transaction.class.php';
require_once 'sql/Connection.class.php';
require_once 'sql/ConnectionFactory.class.php';
require_once 'sql/ConnectionProperty.class.php';
require_once __DIR__.'/../../dto/Rating.class.php';

/*
 * Class that operate on table 'rating'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2014-05-07 19:16
 */
class RatingMySqlDAO{

	public function getAllRatingsInTheLastPeriod($id, $contactIds, $startTime) {
		$contactIds = (($contactIds==null || count($contactIds)==0) ? -1 : implode(',', $contactIds));
		$sql = 'SELECT * FROM rating WHERE fromUserId = ? and createTime > ? and targetContactId in ('.$contactIds.')';
		$sqlQuery = new SqlQuery($sql); 
		$sqlQuery->setNumber($id);
		$sqlQuery->setNumber($startTime);
		return $this->getList($sqlQuery);
	}
	

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return RatingMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM rating WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM rating';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM rating ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param rating primary key
 	 */
	public function delete($id){
		$sql = 'DELETE FROM rating WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param RatingMySql rating
 	 */
	public function insert($rating){
		$sql = 'INSERT INTO rating (isPhotoReal, isPofessionReal, isAgeReal, createTime, updateTime, fromUserId, targetContactId, ratingFace, ratingBody) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);
		//var_dump($rating);
		$sqlQuery->setNumber($rating->isPhotoReal);
		$sqlQuery->setNumber($rating->isPofessionReal);
		$sqlQuery->setNumber($rating->isAgeReal);
		$sqlQuery->set($rating->createTime);
		$sqlQuery->set($rating->updateTime);
		$sqlQuery->setNumber($rating->fromUserId);
		$sqlQuery->setNumber($rating->targetContactId);
		$sqlQuery->setNumber($rating->ratingFace);
		$sqlQuery->setNumber($rating->ratingBody);

		$id = $this->executeInsert($sqlQuery);	
		$rating->id = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param RatingMySql rating
 	 */
	public function update($rating){
		$sql = 'UPDATE rating SET isPhotoReal = ?, isPofessionReal = ?, isAgeReal = ?, createTime = ?, updateTime = ?, fromUserId = ?, targetContactId = ?, ratingFace = ?, ratingBody = ? WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($rating->isPhotoReal);
		$sqlQuery->setNumber($rating->isPofessionReal);
		$sqlQuery->setNumber($rating->isAgeReal);
		$sqlQuery->set($rating->createTime);
		$sqlQuery->set($rating->updateTime);
		$sqlQuery->setNumber($rating->fromUserId);
		$sqlQuery->setNumber($rating->targetContactId);
		$sqlQuery->setNumber($rating->ratingFace);
		$sqlQuery->setNumber($rating->ratingBody);

		$sqlQuery->setNumber($rating->id);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'TRUNCATE TABLE rating';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
	 * Read row
	 *
	 * @return RatingMySql 
	 */
	protected function readRow($row){
		$rating = new Rating();
		
		$rating->id = $row['id'];
		$rating->isPhotoReal = $row['isPhotoReal'];
		$rating->isPofessionReal = $row['isPofessionReal'];
		$rating->isAgeReal = $row['isAgeReal'];
		$rating->createTime = $row['createTime'];
		$rating->updateTime = $row['updateTime'];
		$rating->fromUserId = $row['fromUserId'];
		$rating->targetContactId = $row['targetContactId'];
		$rating->ratingFace = $row['ratingFace'];
		$rating->ratingBody = $row['ratingBody'];

		return $rating;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return RatingMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab) == 0) {
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>