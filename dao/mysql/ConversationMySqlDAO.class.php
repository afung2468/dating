<?php

require_once 'sql/SqlQuery.class.php';
require_once 'sql/QueryExecutor.class.php';
require_once 'sql/Transaction.class.php';
require_once 'sql/Connection.class.php';
require_once 'sql/ConnectionFactory.class.php';
require_once 'sql/ConnectionProperty.class.php';
require_once __DIR__ . '/../../dto/Conversation.class.php';

/*
 * Class that operate on table 'conversation'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2014-05-07 19:16
 */
class ConversationMySqlDAO {

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return ConversationMySql
	 */
	public function load($id) {
		$sql = 'SELECT * FROM conversation WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery -> setNumber($id);
		return $this -> getRow($sqlQuery);
	}

	/**
	 * Get Domain object by user ID
	 *
	 * @param String $uid1, $uid2
	 * @return ConversationMySql
	 */
	public function loadByUid($uid, $updateTime, $length) {
		$sql = "SELECT * FROM conversation where (userId1 = ? OR userId2 = ?) AND updateTime < ? ORDER BY updateTime desc LIMIT 0, $length";
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery -> setNumber($uid);
		$sqlQuery -> setNumber($uid);
		$sqlQuery -> set($updateTime);
		return $this -> getList($sqlQuery);
	}

	/**
	 * Get Domain object by a pair of user IDs
	 *
	 * @param String $uid1, $uid2
	 * @return ConversationMySql
	 */
	public function loadByUidPair($uid1, $uid2) {
		$sql = 'SELECT * FROM conversation WHERE userId1 = ? AND userId2 =?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery -> setNumber($uid1);
		$sqlQuery -> setNumber($uid2);
		return $this -> getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll() {
		$sql = 'SELECT * FROM conversation';
		$sqlQuery = new SqlQuery($sql);
		return $this -> getList($sqlQuery);
	}

	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn) {
		$sql = 'SELECT * FROM conversation ORDER BY ' . $orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this -> getList($sqlQuery);
	}

	/**
	 * Delete record from table
	 * @param conversation primary key
	 */
	public function delete($id) {
		$sql = 'DELETE FROM conversation WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery -> setNumber($id);
		return $this -> executeUpdate($sqlQuery);
	}

	/**
	 * Insert record to table
	 *
	 * @param ConversationMySql conversation
	 */
	public function insert($conversation) {
		$sql = 'INSERT INTO conversation (userId1, userId2, createTime, updateTime, contactId1, contactId2) VALUES (?, ?, ?, ?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);

		$sqlQuery -> setNumber($conversation -> userId1);
		$sqlQuery -> setNumber($conversation -> userId2);
		$sqlQuery -> set($conversation -> createTime);
		$sqlQuery -> set($conversation -> updateTime);
		$sqlQuery -> setNumber($conversation -> contactId1);
		$sqlQuery -> setNumber($conversation -> contactId2);

		$id = $this -> executeInsert($sqlQuery);
		$conversation -> id = $id;
		return $id;
	}

	/**
	 * Update record in table
	 *
	 * @param ConversationMySql conversation
	 */
	public function update($conversation) {
		$sql = 'UPDATE conversation SET userId1 = ?, userId2 = ?, createTime = ?, updateTime = ?, contactId1 = ?, contactId2 = ? WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);

		$sqlQuery -> setNumber($conversation -> userId1);
		$sqlQuery -> setNumber($conversation -> userId2);
		$sqlQuery -> set($conversation -> createTime);
		$sqlQuery -> set($conversation -> updateTime);
		$sqlQuery -> setNumber($conversation -> contactId1);
		$sqlQuery -> setNumber($conversation -> contactId2);

		$sqlQuery -> setNumber($conversation -> id);
		return $this -> executeUpdate($sqlQuery);
	}

	/**
	 * Delete all rows
	 */
	public function clean() {
		$sql = 'TRUNCATE TABLE conversation';
		$sqlQuery = new SqlQuery($sql);
		return $this -> executeUpdate($sqlQuery);
	}

	/**
	 * Read row
	 *
	 * @return ConversationMySql
	 */
	protected function readRow($row) {
		$conversation = new Conversation();

		$conversation -> id = $row['id'];
		$conversation -> userId1 = $row['userId1'];
		$conversation -> userId2 = $row['userId2'];
		$conversation -> createTime = $row['createTime'];
		$conversation -> updateTime = $row['updateTime'];
		$conversation -> contactId1 = $row['contactId1'];
		$conversation -> contactId2 = $row['contactId2'];

		return $conversation;
	}

	protected function getList($sqlQuery) {
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for ($i = 0; $i < count($tab); $i++) {
			$ret[$i] = $this -> readRow($tab[$i]);
		}
		return $ret;
	}

	/**
	 * Get row
	 *
	 * @return ConversationMySql
	 */
	protected function getRow($sqlQuery) {
		$tab = QueryExecutor::execute($sqlQuery);
		if (count($tab) == 0) {
			return null;
		}
		return $this -> readRow($tab[0]);
	}

	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery) {
		return QueryExecutor::execute($sqlQuery);
	}

	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery) {
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery) {
		return QueryExecutor::executeInsert($sqlQuery);
	}

}
?>