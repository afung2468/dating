<?php

require_once 'sql/SqlQuery.class.php';
require_once 'sql/QueryExecutor.class.php';
require_once 'sql/Transaction.class.php';
require_once 'sql/Connection.class.php';
require_once 'sql/ConnectionFactory.class.php';
require_once 'sql/ConnectionProperty.class.php';
require_once __DIR__ . '/../../dto/Contact.class.php';
/*
 * Class that operate on table 'contact'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2014-05-07 19:16
 */
class ContactMySqlDAO {

	/**
	 * Get Domain object by email
	 *
	 * @param String $email
	 * @Return User list with the email
	 */
	public function loadByEmail($email) {
		$sql = 'SELECT * FROM contact WHERE email = ? LIMIT 1';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery -> set($email);
		return $this -> getRow($sqlQuery);
	}

	/**
	 */
	public function loadByUserId($userId) {
		$sql = 'SELECT * FROM contact WHERE userId = ? LIMIT 1';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery -> setNumber($userId);
		return $this -> getRow($sqlQuery);
	}

	public function loadByFbId($fbId) {
		$sql = 'SELECT * FROM contact WHERE fbId = ? LIMIT 1';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery -> setNumber($fbId);
		return $this -> getRow($sqlQuery);

	}

	public function loadByFbIdArray($fbIds) {
		$sql = 'SELECT * FROM contact WHERE fbId in (' . implode(",", $fbIds) . ')';
		$sqlQuery = new SqlQuery($sql);
		return $this -> getList($sqlQuery);

	}

	public function loadByFbIdAndEmailArray($fbIds, $emails) {
		$commaFbIds = (($fbIds == null || count($fbIds) == 0) ? -1 : implode(',', $fbIds));
		$commaEmails = (($emails == null || count($emails) == 0) ? "\"undefine\"" : "\"" . implode("\",\"", $emails) . "\"");
		$sql = 'SELECT * FROM contact WHERE fbId in (' . $commaFbIds . ') or email in (' . $commaEmails . ')';
		$sqlQuery = new SqlQuery($sql);
		return $this -> getList($sqlQuery);

	}

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return ContactMySql
	 */
	public function load($id) {
		$sql = 'SELECT * FROM contact WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery -> setNumber($id);
		return $this -> getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll() {
		$sql = 'SELECT * FROM contact';
		$sqlQuery = new SqlQuery($sql);
		return $this -> getList($sqlQuery);
	}

	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn) {
		$sql = 'SELECT * FROM contact ORDER BY ' . $orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this -> getList($sqlQuery);
	}

	/**
	 * Delete record from table
	 * @param contact primary key
	 */
	public function delete($id) {
		$sql = 'DELETE FROM contact WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery -> setNumber($id);
		return $this -> executeUpdate($sqlQuery);
	}

	/**
	 * Insert record to table
	 *
	 * @param ContactMySql contact
	 */
	public function insert($contact) {
		$sql = 'INSERT INTO contact (email, fbId, userId, birthday, ageVerified, ageNotVerified, profession, professionVerified, professionNotVerified, mainPhotoVerified, mainPhotoNotVerified, createTime, updateTime, sumRatingFace, sumRatingBody, numRatingFace, numRatingBody, gender, numAllVerified, name, mainPhotoId) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);

		$sqlQuery -> set($contact -> email);
		$sqlQuery -> setNumber($contact -> fbId);
		$sqlQuery -> setNumber($contact -> userId);
		$sqlQuery -> set($contact -> birthday);
		$sqlQuery -> setNumber($contact -> ageVerified);
		$sqlQuery -> setNumber($contact -> ageNotVerified);
		$sqlQuery -> set($contact -> profession);
		$sqlQuery -> setNumber($contact -> professionVerified);
		$sqlQuery -> setNumber($contact -> professionNotVerified);
		$sqlQuery -> setNumber($contact -> mainPhotoVerified);
		$sqlQuery -> setNumber($contact -> mainPhotoNotVerified);
		$sqlQuery -> set($contact -> createTime);
		$sqlQuery -> set($contact -> updateTime);
		$sqlQuery -> setNumber($contact -> sumRatingFace);
		$sqlQuery -> setNumber($contact -> sumRatingBody);
		$sqlQuery -> setNumber($contact -> numRatingFace);
		$sqlQuery -> setNumber($contact -> numRatingBody);
		$sqlQuery -> setNumber($contact -> gender);
		$sqlQuery -> setNumber($contact -> numAllVerified);
		$sqlQuery -> set($contact -> name);
		$sqlQuery -> setNumber($contact -> mainPhotoId);

		$id = $this -> executeInsert($sqlQuery);
		$contact -> id = $id;
		return $id;
	}

	/**
	 * Update record in table
	 *
	 * @param ContactMySql contact
	 */
	public function update($contact) {
		$sql = 'UPDATE contact SET email = ?, fbId = ?, userId = ?, birthday = ?, ageVerified = ?, ageNotVerified = ?, profession = ?, professionVerified = ?, professionNotVerified = ?, mainPhotoVerified = ?, mainPhotoNotVerified = ?, createTime = ?, updateTime = ?, sumRatingFace = ?, sumRatingBody = ?, numRatingFace = ?, numRatingBody = ?, gender = ?, numAllVerified = ?, name = ?, mainPhotoId=? WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);

		$sqlQuery -> set($contact -> email);
		$sqlQuery -> setNumber($contact -> fbId);
		$sqlQuery -> setNumber($contact -> userId);
		$sqlQuery -> set($contact -> birthday);
		$sqlQuery -> setNumber($contact -> ageVerified);
		$sqlQuery -> setNumber($contact -> ageNotVerified);
		$sqlQuery -> set($contact -> profession);
		$sqlQuery -> setNumber($contact -> professionVerified);
		$sqlQuery -> setNumber($contact -> professionNotVerified);
		$sqlQuery -> setNumber($contact -> mainPhotoVerified);
		$sqlQuery -> setNumber($contact -> mainPhotoNotVerified);
		$sqlQuery -> set($contact -> createTime);
		$sqlQuery -> set($contact -> updateTime);
		$sqlQuery -> setNumber($contact -> sumRatingFace);
		$sqlQuery -> setNumber($contact -> sumRatingBody);
		$sqlQuery -> setNumber($contact -> numRatingFace);
		$sqlQuery -> setNumber($contact -> numRatingBody);
		$sqlQuery -> setNumber($contact -> gender);
		$sqlQuery -> setNumber($contact -> numAllVerified);
		$sqlQuery -> set($contact -> name);
		$sqlQuery -> setNumber($contact -> mainPhotoId);

		$sqlQuery -> setNumber($contact -> id);
		return $this -> executeUpdate($sqlQuery);
	}

	/**
	 * Delete all rows
	 */
	public function clean() {
		$sql = 'TRUNCATE TABLE contact';
		$sqlQuery = new SqlQuery($sql);
		return $this -> executeUpdate($sqlQuery);
	}

	/**
	 * Read row
	 *
	 * @return ContactMySql
	 */
	protected function readRow($row) {
		$contact = new Contact();

		$contact -> id = $row['id'];
		$contact -> email = $row['email'];
		$contact -> fbId = $row['fbId'];
		$contact -> userId = $row['userId'];
		$contact -> birthday = $row['birthday'];
		$contact -> ageVerified = $row['ageVerified'];
		$contact -> ageNotVerified = $row['ageNotVerified'];
		$contact -> profession = $row['profession'];
		$contact -> professionVerified = $row['professionVerified'];
		$contact -> professionNotVerified = $row['professionNotVerified'];
		$contact -> mainPhotoVerified = $row['mainPhotoVerified'];
		$contact -> mainPhotoNotVerified = $row['mainPhotoNotVerified'];
		$contact -> createTime = $row['createTime'];
		$contact -> updateTime = $row['updateTime'];
		$contact -> sumRatingFace = $row['sumRatingFace'];
		$contact -> sumRatingBody = $row['sumRatingBody'];
		$contact -> numRatingFace = $row['numRatingFace'];
		$contact -> numRatingBody = $row['numRatingBody'];
		$contact -> gender = $row['gender'];
		$contact -> numAllVerified = $row['numAllVerified'];
		$contact -> name = $row['name'];
		$contact -> mainPhotoId = $row['mainPhotoId'];

		return $contact;
	}

	protected function getList($sqlQuery) {
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for ($i = 0; $i < count($tab); $i++) {
			$ret[$i] = $this -> readRow($tab[$i]);
		}
		return $ret;
	}

	/**
	 * Get row
	 *
	 * @return ContactMySql
	 */
	protected function getRow($sqlQuery) {
		$tab = QueryExecutor::execute($sqlQuery);
		if (count($tab) == 0) {
			return null;
		}
		return $this -> readRow($tab[0]);
	}

	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery) {
		return QueryExecutor::execute($sqlQuery);
	}

	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery) {
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery) {
		return QueryExecutor::executeInsert($sqlQuery);
	}

}
?>