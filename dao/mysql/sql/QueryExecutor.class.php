<?php
/*
 * Object executes sql queries
 *
 * @author: http://phpdao.com
 * @date: 27.11.2007
 */
class QueryExecutor{

	/**
	 * Wykonaniew zapytania do bazy
	 *
	 * @param sqlQuery obiekt typu SqlQuery
	 * @return wynik zapytania 
	 */
	public static function execute($sqlQuery){
		$transaction = Transaction::getCurrentTransaction();
		if(!$transaction){
			$connection = new Connection();
		}else{
			$connection = $transaction->getConnection();
		}		
		$query = $sqlQuery->getQuery();
		$result = $connection->executeQuery($query);
		//echo "this query: " . $query;
		if(!$result){
			throw new Exception(mysql_error());
		}
		$i=0;
		$tab = array();
		while ($row = mysql_fetch_array($result)){
			$tab[$i++] = $row;
		}
		mysql_free_result($result);
		if(!$transaction){
			$connection->close();
		}
		return $tab;
	}
	
	/**
	 * Wykonaniew zapytania do bazy
	 *
	 * @param sqlQuery obiekt typu SqlQuery
	 * @return wynik zapytania 
	 * 
	 * This function does not transfer the result into an array to save memory.
	 * The result will be retrieved one by one by the caller as opposed to putting all in a big array.
	 */
	public static function executeForResult($sqlQuery){
		$transaction = Transaction::getCurrentTransaction();
		if(!$transaction){
			$connection = new Connection();
		}else{
			$connection = $transaction->getConnection();
		}		
		$query = $sqlQuery->getQuery();
		$result = $connection->executeQuery($query);
		//echo "this query: " . $query;
		if(!$result){
			throw new Exception(mysql_error());
		}
		return $result;
	}
	
	
	
	public static function executeUpdate($sqlQuery){
		$transaction = Transaction::getCurrentTransaction();
		if(!$transaction){
			$connection = new Connection();
		}else{
			$connection = $transaction->getConnection();
		}		
		$query = $sqlQuery->getQuery();
		$result = $connection->executeQuery($query);
		if(!$result){
			throw new Exception(mysql_error());
		}
		return mysql_affected_rows();		
	}

	public static function executeInsert($sqlQuery){
		//var_dump($sqlQuery);
		QueryExecutor::executeUpdate($sqlQuery);
		return mysql_insert_id();
	}
	
	/**
	 * Wykonaniew zapytania do bazy
	 *
	 * @param sqlQuery obiekt typu SqlQuery
	 * @return wynik zapytania 
	 */
	public static function queryForString($sqlQuery){
		$transaction = Transaction::getCurrentTransaction();
		if(!$transaction){
			$connection = new Connection();
		}else{
			$connection = $transaction->getConnection();
		}
		$query = $sqlQuery->getQuery();
		//echo "query: " . $query . "<br>";
		$result = $connection->executeQuery($query);
		if(!$result){
			throw new Exception(mysql_error());
		}
		return $result;
	}

}
?>