<?php
/*
 * Object represents sql query with params
 *
 * @author: http://phpdao.com
 * @date: 27.10.2007
 */
class SqlQuery{
	var $txt;
	var $params = array();
	var $idx = 0;

	/**
	 * Constructor
	 *
	 * @param String $txt zapytanie sql
	 */
	function SqlQuery($txt){
		$this->txt = $txt;
	}

	/**
	 * Set string param
	 *
	 * @param String $value value set
	 */
	public function setString($value){
		$value = mysql_escape_string($value);
		$this->params[$this->idx++] = "'".$value."'";
	}
	
	/**
	 * Set string param
	 *
	 * @param String $value value to set
	 */
	public function set($value){
		if($value === NULL) {
			$this->params[$this->idx++] = NULL;
			return;
		}
		$value = mysql_escape_string($value);
		$this->params[$this->idx++] = "'".$value."'";
	}
	

	/**
	 * Metoda zamienia znaki zapytania
	 * na wartosci przekazane jako parametr metody
	 *
	 * @param String $value wartosc do wstawienia
	 */
	public function setNumber($value){
		if($value === NULL) {
			$this->params[$this->idx++] = NULL;
			return;
		}

		if(!is_numeric($value)){
			throw new Exception($value.' is not a number');
		}
		$this->params[$this->idx++] = "'".$value."'";
	}

	/**
	 * Get sql query
	 *
	 * @return String
	 */
	public function getQuery(){
		if($this->idx==0){
			return $this->txt;
		}
		$p = explode("?", $this->txt);

		$sql = '';
		$max = sizeof($this->params);
		for($i=0;$i<=$this->idx;$i++){
			$sql .= $p[$i];
			if($max > $i) {
				if($this->params[$i] === NULL)
					$sql .= 'NULL';
				else
					$sql .= $this->params[$i];
			}
		}
		return $sql;
	}
	
	/**
	 * Function replace first char
	 *
	 * @param String $str
	 * @param String $old
	 * @param String $new
	 * @return String
	 */
	private function replaceFirst($str, $old, $new){
		$len = strlen($str);
		for ($i=0;$i<$len;$i++){
			if($str[$i]==$old){
				$str = substr($str,0,$i).$new.substr($str,$i+1);
				return $str;
			}
		}
		return $str;
	}
}
?>