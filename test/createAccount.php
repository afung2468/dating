<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php

	$file_root = dirname(dirname(__FILE__));
	require_once $file_root.'/dto/User.class.php';
	require_once $file_root.'/service/AccountService.class.php';
	require_once $file_root.'/dto/Contact.class.php';
	require_once $file_root."/dao/mysql/ContactMySqlDAO.class.php";
	
	$cities = array("Toronto", "Mississauga", "Brampton", "Richmond Hill", "Pickering", "New Market");
	$ethnicities = array("Asian", "Caucasian", "Black", "Hispanic", "Indian/Pakistani", "Middle Eastern", "Mixed", "Native American", "Other");
	$religions = array("None", "Christian", "Hindu", "Buddhist", "Catholic", "Jewish", "Muslim", "Sikhism", "Other");
	$educations = array("High School", "College", "Master", "PhD");
	$professions = array("Software Engineer", "Accountant", "MD", "Lawyer", "Teacher", "Nurse");
	
	if (isset($_POST['SubOrder'])) {
		echo "Creating 1 accounts, <br>";
		//$nacct = $_POST['nacct'];
		
		$userid = testSignUp();
	
		echo "User ID created: $userid <br>";
	}
	
	//testSignUp();
	//testRecieveEmailConfirmation();
	//testLogin();

	function generateRandomString($length) {
    	$characters = 'abcdefghijklmnopqrstuvwxyz';
    	$randomString = '';
    	for ($i = 0; $i < $length; $i++) {
        	$randomString .= $characters[rand(0, strlen($characters) - 1)];
    	}
    	return $randomString;
	}
	
	function testLogin() {
		$accountService = new AccountService();
		var_dump($accountService->login("marcyliew@gmail.com","1234"));

	}

	function testSignUp() {
		global $cities, $ethnicities, $religions, $educations, $professions;
		//$accName = "test" . generateRandomString(10);
		$accountService = new AccountService();

		$user = new User();
		$user->password = "1234";
		
		$accName = $_POST['userName'];
		$emailName = str_replace(" ", "", $accName);
		$user->email = $emailName . "@abc.com";
		$user->userName = $accName;
		//echo "User name: $user->userName";
		$user->isStudent = rand(0, 1);
		$user->studentEmail = $emailName . "@mail.utoronto.ca";
		$user->institute = $_POST['instit'];
		$user->major = $_POST['major'];
		$user->emailNotificationMessages = 1;
		$user->emailNotificationMatches = 1;
		$user->gender = $_POST['gender'];
		$user->seeking = $_POST['sGender'];
		$user->city = $cities[rand(0, 5)];
		$user->proviceState = "Ontario";
		$user->country = "Canada";
		$user->ethnicity = $_POST['ethnic'];
		$user->religion = $_POST['religion'];
		$user->education = $educations[rand(0, 3)];
		$user->height = $_POST['heig'];
		$user->studentEmailVerified = 1;
		$user->numCoins = 20;
		$user->profession = "student";
		$byear = $_POST['byear'];
		$bmonth = rand(1,12);
		$bday = rand(1,30);
		$user->birthday = $byear . "-$bmonth-$bday";
		$user->aboutMe = $_POST['aboutMe'];
		$user->fbId = rand(1888000000, 1888999999);
		$user->totalLoginTimes = 5;
		$user->lastLoginTime = round(microtime(true) * 1000);
	 	$user->openFacebookLink =0;

		$accountService->signUpUser($user,$user->password,AccountService::FROM_FACEBOOK);
		createContactOfUser($user);
		return $user->id;
	}

	function createContactOfUser($user){
		// check whether there is a contact for him, if so, link the contact to the user
		// if not, create a contact and link it to the user, store the contact to db
		$contactDAO = new ContactMysqlDAO();
		$contact = $contactDAO->loadByEmail($user->email);
		if($contact != null) {
			$contact->userId = $user->id;
			$contact->name = $user->userName;
			$contact->profession = $user->profession;
			$contact->birthday = $user->birthday;
			$contact->gender = $user->gender;
			
			$contact->email = $user->email;
			$contact->ageVerified = rand(0, 5);
			$contact->ageNotVerified = rand(0, 3);	
			$contact->professionVerified = rand(0, 5);
			$contact->professionNotVerified = rand(0, 3);
			$contact->mainPhotoVerified = rand(0, 5);
			$contact->mainPhotoNotVerified = rand(0, 3);
			$contact->createTime = round(microtime(true) * 1000);
			$contact->updateTime = round(microtime(true) * 1000);
			if ($user->gender == 1) {
				$fRate = rand(3, 5);
				$bRate = rand(3, 5);
			} else {
				$fRate = rand(3, 6);
				$bRate = rand(3, 6);
			}
			$contact->numRatingFace = rand(1, 8);
			$contact->numRatingBody = rand(1, 8);
			//echo "fRate,bRate,nFace,nBody: $fRate $bRate $contact->numRatingFace $contact->numRatingBody";
			$contact->sumRatingFace = $contact->numRatingFace * $fRate;
			$contact->sumRatingBody = $contact->numRatingBody * $bRate;
			
			//$contact->numAllVerified = rand(1, 50);
			$contact->numAllVerified = max($contact->numRatingFace, $contact->numRatingBody);

			$contactDAO->update($contact);
		}
		else {
			$contact = new Contact();
			$contact->name = $user->userName;
			$contact->userId = $user->id;
			$contact->gender = $user->gender;
			$contact->profession = $user->profession;
			$contact->birthday = $user->birthday;
			
			$contact->email = $user->email;
			$contact->ageVerified = rand(0, 5);
			$contact->ageNotVerified = rand(0, 3);	
			$contact->professionVerified = rand(0, 5);
			$contact->professionNotVerified = rand(0, 3);
			$contact->mainPhotoVerified = rand(0, 5);
			$contact->mainPhotoNotVerified = rand(0, 3);
			$contact->createTime = round(microtime(true) * 1000);
			$contact->updateTime = round(microtime(true) * 1000);
			
			if ($user->gender == 1) {
				$fRate = rand(3, 5);
				$bRate = rand(3, 5);
			} else {
				$fRate = rand(3, 6);
				$bRate = rand(3, 6);
			}
			$contact->numRatingFace = rand(1, 8);
			$contact->numRatingBody = rand(1, 8);
			//echo "fRate,bRate,nFace,nBody: $fRate $bRate $contact->numRatingFace $contact->numRatingBody";
			$contact->sumRatingFace = $contact->numRatingFace * $fRate;
			$contact->sumRatingBody = $contact->numRatingBody * $bRate;
			
			//$contact->numAllVerified = rand(1, 50);
			$contact->numAllVerified = max($contact->numRatingFace, $contact->numRatingBody);		

			$contactDAO->insert($contact);
		}
	}

	function testRecieveEmailConfirmation() {
		$accountService = new AccountService();
		$user = $accountService->login("marcyliew@gmail.com","1234");
		$accountService->recieveComfirmationEmail($user->email, $user->salt);
	}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>

<p>Creating a fake account v102</p>
<form id="order_detail" action="createAccount.php" method="post">
  <p>
  User Name: 
    <input size="40" name="userName"><br>
    Education Institution: 
    <input size="50" name="instit" value="University of Toronto"><br>
    Major: 
    <select id="MJid" name="major">
      <option></option>
      <option value="Art">Art</option>
      <option value="Physics">Physics</option>
      <option value="Computer">Computer</option>
      <option value="Business">Business</option>
      <option value="Nursing">Nursing</option>
      <option value="Engineering">Engineering</option>
      <option value="Medicine">Medicine</option>
      <option value="Psycology">Psycology</option>
      <option value="Dentistry">Dentistry</option>
    </select><br>
    
    Gender (1 for male, 2 for female): 
    <input size="3" name="gender"><br>
    Seeking (1 for male, 2 for female): 
    <input size="3" name="sGender"><br>
    Height (in cm., 5'=152cm., 5'6"=168cm.): 
    <input size="5" name="heig"><br>
    Age: 
    <select id="AGid" name="byear">
      <option></option>
      <option value="1998">16</option>
      <option value="1997">17</option>
      <option value="1996">18</option>
      <option value="1995">19</option>
      <option value="1994">20</option>
      <option value="1993">21</option>
      <option value="1992">22</option>
      <option value="1991">23</option>
      <option value="1990">24</option>
      <option value="1989">25</option>
      <option value="1988">26</option>
      <option value="1987">27</option>
      <option value="1986">28</option>
      <option value="1985">29</option>
      <option value="1984">30</option>
    </select><br>
    Ethnicity: 
    <select id="ENid" name="ethnic">
      <option></option>
      <option value="White">White</option>
      <option value="Black">Black</option>
      <option value="Asian">Asian</option>
      <option value="Hispanic">Hispanic</option>
      <option value="Native American">Native American</option>
      <option value="Multiracial">Multiracial</option>
    </select><br>
    Religion: 
    <select id="RLid" name="religion">
      <option></option>
      <option value="Christian">Christian</option>
      <option value="Catholic">Catholic</option>
      <option value="Judaism">Judaism</option>
      <option value="Islam">Islam</option>
      <option value="Buddhism">Buddhism</option>
      <option value="Hinduism">Hinduism</option>
      <option value="Jainism">Jainism</option>
      <option value="Sikhism">Sikhism</option>
      <option value="Other">Other</option>
      <option value="No Religion">No Religion</option>   
    </select><br>
    
    About Me: 
    <input size="100" name="aboutMe" value="I am good looking"><br>

    <input value="Submit: Create Account" type="submit" name="SubOrder">
    <br>
  </p>
</form>

</body>
</html>