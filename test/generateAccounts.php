<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php

	$file_root = dirname(dirname(__FILE__));
	require_once $file_root.'/dto/User.class.php';
	require_once $file_root.'/service/AccountService.class.php';
	require_once $file_root.'/dto/Contact.class.php';
	require_once $file_root."/dao/mysql/ContactMySqlDAO.class.php";
	
	$cities = array("Toronto", "Mississauga", "Brampton", "Richmond Hill", "Pickering", "New Market");
	$ethnicities = array("Asian", "Caucasian", "Black", "Hispanic", "Indian/Pakistani", "Middle Eastern", "Mixed", "Native American", "Other");
	$religions = array("None", "Christian", "Hindu", "Buddhist", "Catholic", "Jewish", "Muslim", "Sikhism", "Other");
	$educations = array("High School", "College", "Master", "PhD");
	$professions = array("Software Engineer", "Accountant", "MD", "Lawyer", "Teacher", "Nurse");
	
	if (isset($_POST['SubOrder'])) {
		echo "Creating accounts, please wait  <br>";
		$nacct = $_POST['nacct'];
		
		for ($i = 0; $i < $nacct; $i++) {
			$userid = testSignUp();
		}		
		echo "Last User ID created: $userid <br>";
	}
	
	//testSignUp();
	//testRecieveEmailConfirmation();
	//testLogin();

	function generateRandomString($length) {
    	$characters = 'abcdefghijklmnopqrstuvwxyz';
    	$randomString = '';
    	for ($i = 0; $i < $length; $i++) {
        	$randomString .= $characters[rand(0, strlen($characters) - 1)];
    	}
    	return $randomString;
	}
	
	function testLogin() {
		$accountService = new AccountService();
		var_dump($accountService->login("marcyliew@gmail.com","1234"));

	}

	function testSignUp() {
		global $cities, $ethnicities, $religions, $educations, $professions;
		$accName = "test" . generateRandomString(10);
		$accountService = new AccountService();

		$user = new User();
		$user->password = "1234";

		$user->email = $accName . "@abc.com";
		$user->userName = $accName;
		$user->isStudent = rand(0, 1);
		$user->studentEmail = $accName . "@mail.utoronto.ca";
		$user->institute = "University of Toronto";
		$user->major = "computer engineering";
		$user->emailNotificationMessages = 1;
		$user->emailNotificationMatches = 1;
		$user->gender = rand(1, 2);
		$user->seeking = rand(1, 2);
		$user->city = $cities[rand(0, 5)];
		$user->proviceState = "Ontario";
		$user->country = "Canada";
		$user->ethnicity = $ethnicities[rand(0, 8)];
		$user->religion = $religions[rand(0, 8)];
		$user->education = $educations[rand(0, 3)];
		$user->height = rand(120, 220);
		$user->studentEmailVerified = 0;
		$user->numCoins = 20;
		$user->profession = $professions[rand(0, 5)];
		$byear = rand(1950, 2000);
		$user->birthday = $byear . "-09-08";

		$accountService->signUpUser($user,$user->password,AccountService::FROM_FACEBOOK);
		createContactOfUser($user);
		return $user->id;
	}

	function createContactOfUser($user){
		// check whether there is a contact for him, if so, link the contact to the user
		// if not, create a contact and link it to the user, store the contact to db
		$contactDAO = new ContactMysqlDAO();
		$contact = $contactDAO->loadByEmail($user->email);
		if($contact != null) {
			$contact->userId = $user->id;
			$contact->name = $user->userName;
			$contact->profession = $user->profession;
			$contact->birthday = $user->birthday;
			$contact->gender = $user->gender;
			
			$contact->email = $user->email;
			$contact->ageVerified = rand(0, 50);
			$contact->ageNotVerified = rand(0, 50);	
			$contact->professionVerified = rand(0, 50);
			$contact->professionNotVerified = rand(0, 50);
			$contact->mainPhotoVerified = rand(0, 50);
			$contact->mainPhotoNotVerified = rand(0, 50);
			$contact->createTime = round(microtime(true) * 1000);
			$contact->updateTime = round(microtime(true) * 1000);
			$contact->sumRatingFace = rand(100, 5000);
			$contact->sumRatingBody = rand(100, 5000);
			$contact->numRatingFace = rand(1, 50);
			$contact->numRatingBody = rand(1, 50);
			$contact->numAllVerified = rand(1, 50);

			$contactDAO->update($contact);
		}
		else {
			$contact = new Contact();
			$contact->name = $user->userName;
			$contact->userId = $user->id;
			$contact->gender = $user->gender;
			$contact->profession = $user->profession;
			$contact->birthday = $user->birthday;
			
			$contact->email = $user->email;
			$contact->ageVerified = rand(0, 50);
			$contact->ageNotVerified = rand(0, 50);	
			$contact->professionVerified = rand(0, 50);
			$contact->professionNotVerified = rand(0, 50);
			$contact->mainPhotoVerified = rand(0, 50);
			$contact->mainPhotoNotVerified = rand(0, 50);
			$contact->createTime = round(microtime(true) * 1000);
			$contact->updateTime = round(microtime(true) * 1000);
			$contact->sumRatingFace = rand(100, 5000);
			$contact->sumRatingBody = rand(100, 5000);
			$contact->numRatingFace = rand(1, 50);
			$contact->numRatingBody = rand(1, 50);
			$contact->numAllVerified = rand(1, 50);		

			$contactDAO->insert($contact);
		}
	}

	function testRecieveEmailConfirmation() {
		$accountService = new AccountService();
		$user = $accountService->login("marcyliew@gmail.com","1234");
		$accountService->recieveComfirmationEmail($user->email, $user->salt);
	}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>

<p>Creating random test accounts</p>
<form id="order_detail" action="generateAccounts.php" method="post">
  <p>Number of accounts to createl: 
    <input size="30" maxlength="30" name="nacct"><br />
    <br>
    <input value="Submit: Create Accounts" type="submit" name="SubOrder">
    <br>
  </p>
</form>


</body>
</html>