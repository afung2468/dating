<?php
require_once __DIR__ . "/../../utility/Pusher.php";
require_once __DIR__ . "/../../service/MessageService.class.php";
require_once __DIR__ . "/../../utility/Utility.class.php";
require_once __DIR__ . "/../../utility/Properties.class.php";

session_start();
$id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;

if ($id == null) {
	Utility::message(406, "Your session has expired.");
}

$toId = isset($_GET['toId']) ? $_GET['toId'] : null;
$content = isset($_GET['content']) ? $_GET['content'] : null;
if ($toId == null || $content == null) {
	Utility::message(406, "Parameters error");
}

$messageService = new MessageService();
$message = $messageService -> CreateNewMessage($id, $toId, $content);
$message -> fromMe = 0;

$key = Properties::$PUSHER_KEY;
$secret = Properties::$PUSHER_SECRET;
$app_id = Properties::$PUSHER_APP_ID;

$pusher = new Pusher($key, $secret, $app_id);
$channel = "private-msg-" . $toId;
$pusher -> trigger($channel, "new-message", $message);
?>