<?php
require_once __DIR__ . "/../../utility/Pusher.php";
require_once __DIR__ . "/../../utility/Utility.class.php";
require_once __DIR__ . "/../../utility/Properties.class.php";

$prefix = "private-msg-";

session_start();
$id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;

if ($id == null) {
	Utility::message(406, "Forbidden");
}

if ($prefix . $id == $_POST['channel_name']) {
	$pusher = new Pusher(Properties::$PUSHER_KEY, Properties::$PUSHER_SECRET, Properties::$PUSHER_APP_ID);
	echo $pusher -> socket_auth($_POST['channel_name'], $_POST['socket_id']);
} else {
	Utility::message(406, "Forbidden");
}
?>