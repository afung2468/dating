<?php
require_once __DIR__ . "/../../service/PictureService.class.php";
require_once __DIR__ . "/../../service/AccountService.class.php";
require_once __DIR__ . "/../../service/ContactService.class.php";

session_start();
$uid = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
if ($uid == null) {
	Utility::message(406, "Your session has expired.");
}

$accountService = new AccountService();
$user = $accountService -> load($uid);

if ($user -> accountStatus != 0) {
	Utility::message(406, "You need to confirm your account first.");
}

$contactService = new ContactService();
$contact = $contactService -> loadByUserId($uid);

if (isset($_GET["pid"]) && isset($_GET["visibility"])) {

	$pictureService = new PictureService();
	$pid = $_GET["pid"];
	$visibility = $_GET["visibility"];

	$pic = $pictureService -> load($pid);

	if ($pic -> contactId != $contact -> id) {
		http_response_code(302);
		echo "This is not your picture.";
		exit();
	}

	$pictureService -> changeVisibility($pic -> id, $visibility);

} else {
	Utility::message(406, "Parameters error");
}
?>