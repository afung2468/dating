<?php
    
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";
    $AccountService = new AccountService();

    $email = $_GET["email"];

    $unique = !$AccountService->checkIfUserExists($email);
    if($unique)
        echo 1;
    else
        echo 0;
?>