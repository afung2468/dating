<?php
	require_once __DIR__.'/../../dto/SearchCriteria.class.php';
	require_once __DIR__.'/../../service/SearchService.class.php';
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../service/ContactService.class.php";
    require_once __DIR__."/../../service/EmailService.class.php";
    require_once __DIR__."/../../service/InstituteService.class.php";
    require_once __DIR__."/../../utility/Utility.class.php";

 	session_start();
    $id = isset($_SESSION['user_id'])? $_SESSION['user_id']: null;
    if($id == null) {
    	Utility::message(406,"Your session has expired.");
    }

	$accountService = new AccountService();
	$contactService = new ContactService();
	$instituteService = new InstituteService();
	
	$user = $accountService->load($id);
	$contact = $contactService->loadByUserId($id);
	
	if($user->accountStatus != 0) {
    	Utility::message(406,"You need to confirm your account first.");
    }
	
	try {
	    if(isset($_POST["userName"])) {
	    	$user->userName = $_POST["userName"];
			$accountService->update($user);
			
			$contact->name = $user->userName;
			$contactService->update($contact);
	    }
		else if(isset($_POST["birthYear"]) && isset($_POST["birthDate"]) && isset($_POST["birthMonth"])) {
	    	
	        $date = new DateTime();
	        $date->setDate($_POST["birthYear"], $_POST["birthMonth"], $_POST["birthDate"]);
	        $user->birthday = $date->format('Y-m-d');
			$accountService->update($user);
			
			$contact->birthday = $user->birthday;
			$contact->ageVerified = 0;
			$contact->ageNotVerified = 0;
			$contactService->update($contact);
		}
		else if(isset($_POST["profession"])) {
			$user->profession = $_POST["profession"];
	        $user->isStudent = 0;
			
			if($_POST["profession"] == "student" && isset($_POST["major"]) && isset($_POST["program"]) && isset($_POST["studentEmail"])) {
	    		$user->major = $_POST["major"];
	            $user->isStudent = 1;
	    		$user->education = $_POST["program"];
				
				if($_POST["studentEmail"] != null && $user->studentEmail != $_POST["studentEmail"]) {
					
					$instituteService = new InstituteService();
					if(!$instituteService->isStudentEmailValid($_POST["studentEmail"])) {
						throw new Exception("This email domain is not in our system. Please contact us to add your school email.");
					}
					
					if($_POST["studentEmail"] != $user->studentEmail && $accountService->checkIfStudentExists($_POST["studentEmail"])) {
						throw new Exception("This student email is in use by another user.");
					}
					
	    			$user->studentEmail = $_POST["studentEmail"];
					$user->institute = $instituteService->getSchoolFromEmail($user->studentEmail);
					$user->studentEmailVerified = 0;
					
 					$confirmLink = Properties::$CONFIRM_STUDENT_EMAIL_ACTION."?email=".$user->studentEmail."&code=".$user->salt;
 					$emailService->sendStudentConfirmation($user->studentEmail, $confirmLink);
				}
			}
			
			$accountService->update($user);
			
			$contact->profession = $user->displayProfession();
			$contact->professionVerified = 0;
			$contact->professionNotVerified = 0;
			$contactService->update($contact);
		}
		else if(isset($_POST["country"]) && isset($_POST["city"]) && isset($_POST["postal"])) {
			
	    	$user->country = $_POST["country"];
	    	$user->city = $_POST["city"];
			$user->zipPostalCode =  $_POST["postal"];
			$accountService->update($user);
		}
		else if(isset($_POST["ethnicity"])) {
			
	    	$user->ethnicity = $_POST["ethnicity"];
			$accountService->update($user);
		}
		else if(isset($_POST["religion"])) {
			
	    	$user->religion = $_POST["religion"];
			$accountService->update($user);
		}
		else if(isset($_POST["about-me"])) {
			
	    	$user->aboutMe = $_POST["about-me"];
			$accountService->update($user);
		}
		else if(isset($_POST["new-password"]) && isset($_POST["password"])) {
			
			$accountService->changePassword($user->id, $_POST["password"], $_POST["new-password"]);
		}
		else if(isset($_POST["open-facebook-link"])) {
			
	    	$user->openFacebookLink = $_POST["open-facebook-link"];
			$accountService->update($user);
		}
		else if(isset($_POST["interest"])) {
			
	    	$user->seeking = $_POST["interest"];
			$accountService->update($user);
		}
		else if(isset($_POST["intent"])) {
			
	    	$user->intent = $_POST["intent"];
			$accountService->update($user);
		}
	}
	catch(Exception $e) {
    	Utility::message(406,$e->getMessage());
    }
?>