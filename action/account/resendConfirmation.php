<?php
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../dto/User.class.php";
    require_once __DIR__."/../../utility/Utility.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";
    require_once __DIR__."/../../service/EmailService.class.php";

    try {
        $id = $_GET["id"];
        $accountService = new AccountService();
        $user = $accountService->load($id);
		
		if($user->studentEmail == null) {
        	Utility::redirect(Properties::$MESSAGE_PAGE."?msg=Student email not set",false);
		}

        $emailService = new EmailService();
        $confirmLink = Properties::$CONFIRM_EMAIL_ACTION."?email=".$user->studentEmail."&code=".$user->salt;
        $emailService->sendConfirmation($user->email, $confirmLink);


        $msg = "The confirmation has been resent to your email."
            ." Please click the link inside to activate your account."
            ."<br>Still no email recieved? Check your spam or click <a href='".Properties::$RESEND_CONFIRMATION_ACTION."?id=".$id."'>here</a> to recieve the email again.";

        Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$msg,false);

    }
    catch(Exception $e) {
        Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$e->getMessage(),false);
    }
?>