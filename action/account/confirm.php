<?php
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../dto/User.class.php";
    require_once __DIR__."/../../utility/Utility.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";

    try {
        $salt = $_GET["code"];
        $email = $_GET["email"];

        $accountService = new AccountService();
        $user = $accountService->recieveComfirmationEmail($email, $salt);

        session_start();
        $_SESSION['user_id']=$user->id;

        Utility::redirect(Properties::$PHOTOS_PAGE."?",false);
    }
    catch(Exception $e) {
        Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$e->getMessage(),false);
    }
?>