<?php
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";
	require_once __DIR__."/../../service/InstituteService.class.php";
    require_once __DIR__."/../../utility/Utility.class.php";
	
    $accountService = new AccountService();
	$instituteService = new InstituteService();

	if(!isset($_GET["email"])){
    	Utility::message(406,"Parameters Error");
	}

    $email = $_GET["email"];

	if(!$instituteService->isStudentEmailValid($email)) {
    	Utility::message(406,"This email domain does not match any school in the system. Please contact support@velidate.com to add your school.");
	}

    $unique = !$accountService->checkIfStudentExists($email);
    if(!$unique) {
    	Utility::message(406,"This student email is already registered.");
	}
?>