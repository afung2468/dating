<?php
    
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../utility/Utility.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";

    $AccountService = new AccountService();

    $email = $_GET["email"];
    $password = $_GET["password"];

    $user;
    try {
    	$user = $AccountService->login($email, $password);

    	session_start();
		// store session data
		$_SESSION['user_id']=$user->id;
		echo 1;
    }
    catch (Exception $e) {
    	Utility::message(406, $e->getMessage());
    }
?>