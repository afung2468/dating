<?php
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../dto/User.class.php";
    require_once __DIR__."/../../utility/Utility.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";

    try {
        $salt = $_GET["code"];
        $studentEmail = $_GET["email"];

        $accountService = new AccountService();
        $user = $accountService->recieveStudentComfirmationEmail($studentEmail, $salt);

		$msg = "Student email confirmed sucessfully.";
		if($user->accountStatus == User::CONFIRMED) {
        	Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$msg."Click <a href='".Properties::$PROFILE_PAGE_RELATIVE."'>here</a> to go to your profile page.",false);
		}
		else {
			Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$msg."But you account is not activated yet. Another email was sent to ".$user->email.". Please click the link in the email to activate your account.<br>No email recieved? click <a href='" . Properties::$RESEND_CONFIRMATION_ACTION . "?id=" . $user->id . "'>here</a> to recieve the email again.",false);
		}
		
    }
    catch(Exception $e) {
        Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$e->getMessage(),false);
    }
?>