<?php
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../dto/User.class.php";
    require_once __DIR__."/../../utility/Utility.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";
    require_once __DIR__."/../../service/EmailService.class.php";

    try {
        $id = $_GET["id"];
        $accountService = new AccountService();
        $user = $accountService->load($id);

        $emailService = new EmailService();
        $confirmLink = Properties::$CONFIRM_STUDENT_EMAIL_ACTION."?email=".$user->studentEmail."&code=".$user->salt;
        $emailService->sendStudentConfirmation($user->studentEmail, $confirmLink);

        $msg = "The verification was resent to your student email - ".$user->studentEmail
            ."<br>Still no email recieved? Check your spam or click <a href='".Properties::$RESEND_STUDENT_CONFIRMATION_ACTION."?id=".$id."'>here</a> to recieve the email again."
			."<br>Want to change the email address? Go to <a href='".Properties::$EDIT_PROFILE_PAGE_RELATIVE."'>Edit Page</a> to reset your student email.";

        Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$msg,false);
    }
    catch(Exception $e) {
        Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$e->getMessage(),false);
    }
?>